import sys

sys.path.append('../../api')
sys.path.append('../../settings')
from configuration import *
import numpy as np
import cv2
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger
import datetime
import VisionTools as vT
from multiDropletTracking import MultiDropletTracking
import math
import time
from worldcor import WorldCor

# droplet decanol (volumes)
decanolVol = 1.5
airVol = 0
# salt volume
saltVol = 3

################################################################################
############           Positions of the different vessels           ############
################################################################################

salt_stock = (140, 90)  # position of salt well on experimental space -same of decanol because is pulled with diffsyrin
decanol_stock = (160, 0)  # position of decanol well on experimental space
ph_switcher = {
    7: (100, 0),  # Ph 7 in pos 100, 0
    8: (100, 50),  # Ph 8 in pos 100, 50
    9: (100, 110),  # Ph 9 ...
    10: (100, 160),  # Ph 10 ...
    11: (60, 210),  # Ph 11 ...
    12: (150, 50),  # Ph 12 ...
    13: (140, 0),  # Ph 13 ...
}
water_position = (0, 210)
petridish_center = (180, 125)

# Positions of where to dispense the salt and the decanol droplet  #######
salt_position = (155, 3)  # salt position on petridish
decanol_position = (115, 120)  # decanol position on petridish
park_head_pos = (150, 195)

decanoate_plunger_fast_speed = 150

"""video recording variables"""
date = ""
# fourCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v') ###Use this in mac?
fourCC = cv2.VideoWriter_fourcc(*'XVID')  ###Use this in windows
recording = True  # This activates the recording of the experiment

# Timestamps of the tracking
start_tracking_time = None
end_tracking_time = None

# This resolution should be the camera resolution
window_size = (1280, 720)

"""  Variables for red circle used to delineate the area of the experiment.  """
"""  This is used to make a mask and ignore everitjong ouside the circle     """
"""  It should match with the Petri dish                                     """
ix = 640
iy = 408
R = 130

# Name for the datalogger
fileName = time.strftime("%Y-%m-%d %H%M%S")


class DropletB:
    """        Init function used to initialize all the variables             """

    def __init__(self):
        self.usrmsglogger = DataLogger()
        self.evobot = EvoBot(PORT_NO, self.usrmsglogger)
        self.head = Head(self.evobot)
        self.headLogger = DataLogger('experiments/head' + fileName, kind='csv')
        self.head.setSpeed(9000)
        self.decanol_syringe = Syringe(self.evobot, SYRINGE0)
        self.decanol_syringe.homeSyringe()
        self.decanol_syringe.plungerMovePos(40)
        self.salt_syringe = Syringe(self.evobot, SYRINGE1)
        self.salt_syringe.homeSyringe()
        self.salt_syringe.plungerMovePos(30)
        self.salt_syringe.plungerSetSpeed(24)
        self.salt_syringe.plungerSetAcc(24)
        self.decanoate_syringe = Syringe(self.evobot, SYRINGE2)
        self.decanoate_syringe.homeSyringe()
        self.decanoate_syringe.plungerSetSpeed(decanoate_plunger_fast_speed)
        self.decanoate_syringe.plungerSetAcc(72)
        self.decanoate_syringe.plungerMovePos(40)
        self.syringeLogger = DataLogger('experiments/syringe' + fileName)
        self.decanoate_syringe.dataLogger = self.syringeLogger
        self.head.home()
        self.trasform_matrixes()
        # Set the decanol syringe as the coordinate system of the program
        self.worldcor = WorldCor(self.decanol_syringe, mode='default')

    def trasform_matrixes(self):
        global decanol_matrix, decanol_matrix_transformed, salt_matrix, salt_matrix_transformed
        decanol_matrix = self.decanol_syringe.affineMat
        decanol_matrix_transformed = cv2.invertAffineTransform(decanol_matrix)
        salt_matrix = self.salt_syringe.affineMat
        salt_matrix_transformed = cv2.invertAffineTransform(salt_matrix)

    @staticmethod
    def maskFrame(frame, mask):
        global R
        mask.fill(0)
        cv2.circle(mask, (ix, iy), R, (255, 255, 255), -1)
        frame = cv2.bitwise_and(frame, mask)
        return frame

    ### Function used to pull the decanol     
    def decanol_pull(self):
        self.head.move(decanol_stock[0], decanol_stock[1])
        if self.decanol_syringe.canAbsorbVol(decanolVol):
            self.decanol_syringe.syringeSetSpeed(100)
            # self.decanol_syringe.syringeSetAcc(200)
            self.decanol_syringe.syringeMove(-47)
            self.decanol_syringe.plungerPullVol(decanolVol)
            self.decanol_syringe.syringeMove(0)
            self.decanol_syringe.plungerPullVol(airVol)
        else:
            print "volume exceeds capacity"

    ### Function used to push the decanol            
    def decanol_push(self, x, y):
        self.head.move(decanol_position[0], decanol_position[1])
        # self.decanol_syringe.plungerSetSpeed(150)
        self.decanol_syringe.syringeMove(-47)
        self.decanol_syringe.plungerPushVol(decanolVol + airVol)
        time.sleep(2)
        self.decanol_syringe.syringeMove(0)

        print "volume exceeds capacity"
        print x, y

    ### Function used to pull the salt    
    def salt_pull(self):
        self.head.move(salt_stock[0], salt_stock[1])
        if self.salt_syringe.canAbsorbVol(saltVol):
            self.salt_syringe.syringeMove(-61)
            self.salt_syringe.plungerPullVol(saltVol)
            self.salt_syringe.syringeMove(0)
        else:
            print "volume exceeds capacity"

    ### Function used to push the salt    
    def salt_push(self, x, y):
        self.head.move(salt_position[0], salt_position[1])
        self.salt_syringe.syringeMove(-55)
        self.salt_syringe.plungerPushVol(saltVol)
        self.salt_syringe.syringeMove(0)

        print x, y

    ### Function used to pull  and push the water    
    def addWater(self, water_amount):
        syringeHeight = -20
        # get water
        self.head.move(water_position[0], water_position[1])
        self.decanoate_syringe.syringeMove(syringeHeight)
        self.decanoate_syringe.plungerPullVol(water_amount)
        self.decanoate_syringe.syringeMove(0)

        # dispense water
        self.head.move(petridish_center[0], petridish_center[1])
        self.decanoate_syringe.syringeMove(syringeHeight)
        self.decanoate_syringe.plungerSetSpeed(24)
        self.decanoate_syringe.plungerPushVolNonBlocking(water_amount)

        # Move the head in circles for a long time to guarantee that the move plunger commans has finished
        R = 25
        increasing = True
        RMax = 30
        start_time = datetime.datetime.now()
        now = datetime.datetime.now()
        while ((start_time + datetime.timedelta(seconds=5)) >= now):

            for itr in range(1, 45):
                if increasing:
                    R += float(float(RMax) / 45) * 2
                else:
                    R -= float(float(RMax) / 45) * 2
                if R > RMax:
                    increasing = False
                if R < 0:
                    increasing = True

                angle = (float(itr) / float(45)) * float(8 * math.pi)
                xPos = round(petridish_center[0] + R * math.cos(angle), 2)
                yPos = round(petridish_center[1] + R * math.sin(angle), 2)
                if xPos > 185:
                    xPos = 185
                self.head.moveContinously(xPos, yPos)
                now = datetime.datetime.now()

        # mixing water in petri
        self.head.move(petridish_center[0], petridish_center[1])
        self.decanoate_syringe.syringeMove(syringeHeight - 1)
        self.decanoate_syringe.plungerSetSpeed(decanoate_plunger_fast_speed)
        count = 0
        while count < 3:
            self.decanoate_syringe.plungerPullVol(15)
            self.decanoate_syringe.plungerPushVol(15)
            count += 1
        self.decanoate_syringe.plungerMovePos(40)
        self.decanoate_syringe.syringeMove(0)

    # Function used to pull and push the decanoate
    def getDecanoate(self, x, y, amount):
        syringeHeight = -18

        # get decanoate solution
        self.head.move(x, y)
        self.decanoate_syringe.syringeMove(syringeHeight)
        self.decanoate_syringe.plungerPullVol(amount)
        self.decanoate_syringe.syringeMove(0)

        # dispense decanoate
        self.head.move(petridish_center[0], petridish_center[1])
        self.decanoate_syringe.syringeMove(syringeHeight)
        self.decanoate_syringe.plungerSetSpeed(24)
        self.decanoate_syringe.plungerPushVolNonBlocking(amount)

        # Move the head in circles for a long time to guarantee that the move plunger commands has finished
        R = 25
        increasing = True
        RMax = 30
        start_time = datetime.datetime.now()
        now = datetime.datetime.now()
        while ((start_time + datetime.timedelta(seconds=20)) >= now):

            for itr in range(1, 45):
                if increasing:
                    R += float(float(RMax) / 45) * 2
                else:
                    R -= float(float(RMax) / 45) * 2
                if R > RMax:
                    increasing = False
                if R < 0:
                    increasing = True

                angle = (float(itr) / float(45)) * float(8 * math.pi)
                xPos = round(petridish_center[0] + R * math.cos(angle), 2)
                yPos = round(petridish_center[1] + R * math.sin(angle), 2)
                if xPos > 185:
                    xPos = 185
                self.head.moveContinously(xPos, yPos)
                now = datetime.datetime.now()
        self.decanoate_syringe.syringeMove(0)
        self.decanoate_syringe.plungerSetSpeed(decanoate_plunger_fast_speed)

    def prepare_experiment(self, decanoate_solution):
        ph, molarity = decanoate_solution
        print "Evaluation of the individual (pH parameter is " + str(ph) + " and molarity parameter is " + str(
            molarity) + ")"
        ph_real = 7 + round((13 - 7) * ph)  # give me a number between 7 and 13
        mol_real = 5 + ((20 - 5) * molarity)  # give me a number between 5 and 20
        vol_total = 37  # TODO change according to syringe volume
        vol_decanoate = (0.75 * molarity + 0.25) * vol_total
        print vol_decanoate
        vol_water = vol_total - vol_decanoate

        vessel_pos = ph_switcher.get(ph_real, )

        print "Thus, preparing individual with pH " + str(ph_real) + " and molarity " + str(mol_real)
        print "Adding decanoate"
        self.getDecanoate(vessel_pos[0], vessel_pos[1], vol_decanoate)
        print "Adding water"
        self.addWater(vol_water)

    def perform_experiment(self):
        cv2.namedWindow('Schermata')
        cap = cv2.VideoCapture(CAMERA_ID)
        cap.set(3, window_size[0])
        cap.set(4, window_size[1])
        ret, frame = cap.read()
        if ret == 0:
            print 'ERROR READING INPUT'

        x, y, z = frame.shape
        mask = np.zeros((x, y, 3), np.uint8)

        start_tracking_time = None

        ###If required the speed of the head of the robot could be decreased 
        ### with this line to avoid vobrations
        self.head.setSpeed(9000)

        self.salt_pull()
        self.decanol_pull()

        print "Pushing decanol"
        self.decanol_push(decanol_position[0], decanol_position[1])
        print "Pushing salt"
        self.salt_push(salt_position[0], salt_position[1])

        self.head.move(park_head_pos[0], park_head_pos[1])
        modality = "tracking"
        print "TRACKING MODE."
        # We start an exp!!!

        if recording:
            date = str(datetime.datetime.now())
            import os
            file_name = os.path.join(FILE_PATH, "%s_video.avi" % vT.removeColon(date))
            out = cv2.VideoWriter(file_name, fourCC, 10.0, window_size)

        start_tracking_time = datetime.datetime.now()
        track2 = MultiDropletTracking()

        while True:
            ret, frame = cap.read()  # Read a new frame from the camera
            if ret == 0:
                print "ERROR READING INPUT"
            result = frame.copy()
            cv2.circle(result, (ix, iy), R, (0, 0, 255), 2)
            # cv2.setMouseCallback('Schermata', self.onClick, param=modality)
            frame = self.maskFrame(frame, mask)

            # OPTIONS
            # Waits up to other 30ms for a keypress
            # If none -> -1
            key = cv2.waitKey(30) & 0xFF

            now = datetime.datetime.now()
            done_tracking = modality == "tracking" and ((start_tracking_time + datetime.timedelta(seconds=60)) <= now)

            if done_tracking:
                print "Stop", done_tracking, start_tracking_time, now
                end_tracking_time = now
                break
            elif key == ord('q'):
                print "Aborted", done_tracking, start_tracking_time, now
                end_tracking_time = now
                break

            # Track the droplets
            track2 = MultiDropletTracking(frame, decanol_matrix)

            if modality == "tracking":
                cv2.putText(result, "TRACKING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                track2.do_track(frame, result)
            # elif modality == "setting":
            #                cv2.putText(result, "SETTING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            #                cv2.circle(result, (ix, iy), R, (0, 0, 255), 2)
            #            elif modality == "moving":
            #                cv2.putText(result, "MOVING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)

            if recording:
                out.write(result)
                cv2.putText(result, "RECORDING", (150, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            cv2.imshow('Schermata', result)

        # END WHILE TRUE

        initial_point, ending_point = track2.show_output_biggest(result)

        point = np.float32([initial_point[0], initial_point[1], 1])
        RobCor = np.dot(cv2.invertAffineTransform(decanol_matrix), point)
        ending_point = (RobCor[0], RobCor[1])
        print "Coordinates of the initial point of the droplet (mm): " + str(initial_point)

        point = np.float32([ending_point[0], ending_point[1], 1])
        RobCor = np.dot(cv2.invertAffineTransform(decanol_matrix), point)
        ending_point = (RobCor[0], RobCor[1])
        print "Coordinates of the end point of the droplet (mm): " + str(ending_point)

        print "Coordinates of the initial position of the decanol: " + str(decanol_position)
        print "original salt pos: " + str(salt_position)
        saltPosInDecanolCS = self.worldcor.inverseWorldCorFor(salt_position, self.salt_syringe)
        print "Coordinates of the salt pos in the Decanol Coordinate System (mm): " + str(saltPosInDecanolCS)

        if recording:
            import os
            file_name = os.path.join(FILE_PATH, "%s_screen.jpg" % vT.removeColon(date))
            cv2.imwrite(file_name, result)
            out.release()
        cv2.imshow("Schermata", result)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        self.evobot.printcore.disconnect()

        import csv
        with open('paths/input-{}.csv'.format(start_tracking_time.strftime("%Y-%m-%d %H.%M.%S")), 'wb') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow([
                'X salt',
                'Y salt',
                'X droplet Start',
                'Y droplet Start',
                'X droplet End',
                'Y droplet End',
                'time',
            ])
            tracking_time = (end_tracking_time - start_tracking_time).total_seconds()

            if salt_position is None or salt_position is None:
                print "maybe you should put on the petri the ingredients"

            writer.writerow([
                salt_position[0],
                salt_position[1],
                decanol_position[0],
                decanol_position[1],
                ending_point[0],
                ending_point[1],
                tracking_time,
            ])
        print('----->', decanol_position, ending_point, salt_position, tracking_time)

        def _calculate_pitagora(a, b):
            return math.sqrt(a ** 2 + b ** 2)

        fitness_value = (_calculate_pitagora(ending_point[0] - saltPosInDecanolCS[0],
                                             ending_point[1] - saltPosInDecanolCS[1])) \
                        / (_calculate_pitagora(decanol_position[0] - ending_point[0],
                                               decanol_position[1] - ending_point[1]))

        print fitness_value
        return fitness_value

    def quit(self):
        self.evobot.disconnect()
        self.head.dataLogger.file.close()
        self.decanoate_syringe.dataLogger.file.close()
