import sys
import random
from dropletB import DropletB
from deap import base
from deap import creator
from deap import tools
import pickle

sys.path.append('../../api')
sys.path.append('../../settings')
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger
from configuration import *


creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

IND_SIZE = 2

toolbox = base.Toolbox()
toolbox.register("attribute", random.random)

toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attribute, n=IND_SIZE)

toolbox.register("population", tools.initRepeat, list, toolbox.individual)


# the goal ('fitness') function to be minimized
def evalMin(individual):
    

    repeat = True
    while repeat:
        print "INIT DropletB"
        droplet_tr = DropletB()
        print "Preparing experiment"
        droplet_tr.prepare_experiment(individual)
        print "Experiment prepared, starting experiment"
        fitness = droplet_tr.perform_experiment(),
        print "Experiment has finished"
        print "Check that all the vessels have enough reagents!"
        print "Is everything OK? Type ""y"" to evaluate the next individual or type ""n"" to repeat the evaluation of this individual"
        text = sys.stdin.readline()
        if "y" in text:
            repeat = False
    
    return fitness

    # print "how is the fitness value of the individual?"
    # fitness = float(sys.stdin.readline())
    # return fitness,

def homeRobot():
    usrmsglogger = DataLogger()
    evobot = EvoBot(PORT_NO, usrmsglogger)
    head = Head(evobot)
    decanol_syringe = Syringe(evobot, SYRINGE0)
    salt_syringe = Syringe(evobot, SYRINGE1)
    decanoate_syringe = Syringe(evobot, SYRINGE2)
    evobot.home()
    evobot.disconnect()
    
    
    
toolbox.register("evaluate", evalMin)

toolbox.register("mate", tools.cxTwoPoint)

toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)

toolbox.register("select", tools.selTournament, tournsize=2)


def main(population_file=None):
    # CXPB  is the probability with which two individuals
    #       are crossed
    #
    # MUTPB is the probability for mutating an individual
    #
    # NGEN  is the number of generations for which the
    #       evolution runs
    CXPB, MUTPB, NGEN = 0.8, 0.1, 20
    
    homeRobot()

    random.seed(3)

    if not population_file:
        # create an initial population of 300 individuals (where
        # each individual is a list of floats)
        pop = toolbox.population(n=4)  # enough per noi anche - o mettere a 5
        start_gen = 0
        # Evaluate the entire population
        fitnesses = list(map(toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit,
    else:
        cp = pickle.load(open(population_file, "r"))
        pop = cp["population"]
        start_gen = cp["generation"]
        random.setstate(cp["rndstate"])
        fitnesses = cp["fitness"]
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit,

    print("Start of evolution")

    # Begin the evolution
    for g in range(start_gen, NGEN):
        print("-- Generation %i --" % g)

        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop))
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))

        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):

            # cross two individuals with probability CXPB
            if random.random() < CXPB:
                toolbox.mate(child1, child2)

                # fitness values of the children
                # must be recalculated later
                del child1.fitness.values
                del child2.fitness.values

        for mutant in offspring:

            # mutate an individual with probability MUTPB
            if random.random() < MUTPB:
                toolbox.mutate(mutant)
                del mutant.fitness.values

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        print("  Evaluated %i individuals" % len(invalid_ind))

        # The population is entirely replaced by the offspring
        pop[:] = offspring

        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in pop]

        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x * x for x in fits)
        std = abs(sum2 / length - mean ** 2) ** 0.5

        print("  Min %s" % min(fits))
        print("  Max %s" % max(fits))
        print("  Avg %s" % mean)
        print("  Std %s" % std)

        cp = dict(population=pop, generation=g, fitness=fits, rndstate=random.getstate())
        filename = "population-g" + str(g) + ".pkl"
        pickle.dump(cp, open(filename, "wb"))

    print("-- End of (successful) evolution --")

    best_ind = tools.selBest(pop, 1)[0]
    print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()
