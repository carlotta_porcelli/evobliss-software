import numpy as np
import cv2
import math
import time
import sys
import datetime
import VisionTools as vT
# sys.path.append('../api')
# sys.path.append('../settings')
from settings import CAMERA_ID, PORT_NO, FILE_PATH
# from configuration import *
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger
from tracking import Tracking

from beaker_coordinates import *

salt_position = (145, 3)  # salt position in petridish
decanol_position = (103, 120)  # decanol position in petridish
salt_stock = (140, 90)  # position of salt well on experimental space - same of decanol because is pulled with diffsyrin
decanol_stock = (160, 0)  # position of decanol well on experimental space
petridish_center = (165, 125)

#
# salt_position = (75, 193)  # salt position on petridish
# decanol_position = (95, 193)  # decanol position on petridish
# salt_stock = (120, 160)  # position of salt well on experimental space -same of decanol because
#   is pulled with different syringe
# decanol_stock = (140, 120)  # position of decanol well on experimental space
# decanoate_positions = [[140, 60], [140, 70], [140, 80], [140, 90],
#                        [140, 100], [140, 110], [140, 120]]  # positions ph dal 7 al 13
# water_position = (50, 180)
#
# petridish_center = (170, 170)

"""video recording variables"""
date = ""
fourCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
recording = False

start_tracking_time = None
end_tracking_time = None
window_size = (1280, 720)

"""variables for red circle used to delineate the area of the experiment"""
ix = 640
iy = 408
R = 120

fileName = time.strftime("%Y-%m-%d %H%M%S")


def compute_volumes(x_sol_ph, y_sol_ph, final_molarity, final_ph):
    x_molarity = 20e-3
    # y_molarity = x_molarity
    final_volume = 9e-3
    h2o_ph = 6
    f = final_molarity * 10 ** (-3)
    final_oh_concentration = 1.0 / (10 ** (14 - final_ph))
    x_oh_concentration = 1.0 / (10 ** (14 - x_sol_ph))
    y_oh_concentration = 1.0 / (10 ** (14 - y_sol_ph))
    h2o_oh_concentration = 1.0 / (10 ** (14 - h2o_ph))
    y_vol = (final_oh_concentration * final_volume + (f * final_volume / x_molarity) * h2o_oh_concentration -
             (f * final_volume / x_molarity) * x_oh_concentration - final_volume * h2o_oh_concentration) / \
            (y_oh_concentration - x_oh_concentration)
    x_vol = ((f * final_volume) / x_molarity) - y_vol
    if x_vol < 0:
        if y_sol_ph < 13:
            x_vol, y_vol = compute_volumes(x_sol_ph, y_sol_ph + 1, final_molarity, final_ph)
    elif y_vol < 0:
        if x_sol_ph > 7:
            x_vol, y_vol = compute_volumes(x_sol_ph - 1, y_sol_ph, final_molarity, final_ph)
    return x_vol, y_vol


class DropletB:
    def __init__(self):
        self.usrmsglogger = DataLogger()
        self.evobot = EvoBot(PORT_NO, self.usrmsglogger)
        self.head = Head(self.evobot)
        self.headLogger = DataLogger('experiments/head' + fileName, kind='csv')
        self.head.setSpeed(2500)

        self.sockets = self.evobot.getPopulatedSockets()
        self.decanol_syringe = Syringe(self.evobot, SYRINGE0)
        self.decanol_syringe.syringeMove(0)
        self.decanol_syringe.plungerMovePos(40)
        self.salt_syringe = Syringe(self.evobot, SYRINGE1)
        self.salt_syringe.syringeMove(0)
        self.salt_syringe.plungerMovePos(30)
        self.decanoate_syringe = Syringe(self.evobot, SYRINGE2)
        self.decanoate_syringe.syringeMove(0)
        self.decanoate_syringe.plungerMovePos(40)

        self.syringeLogger = DataLogger('experiments/syringe' + fileName)
        self.decanoate_syringe.dataLogger = self.syringeLogger

        self.head.home()
        self.transform_matrices()

    def transform_matrices(self):
        global decanol_matrix, decanol_matrix_transformed, salt_matrix, salt_matrix_transformed
        decanol_matrix = self.decanol_syringe.affineMat
        decanol_matrix_transformed = cv2.invertAffineTransform(decanol_matrix)
        salt_matrix = self.salt_syringe.affineMat
        salt_matrix_transformed = cv2.invertAffineTransform(salt_matrix)

    @staticmethod
    def mask_frame(frame, mask):
        global R
        mask.fill(0)
        cv2.circle(mask, (ix, iy), R, (255, 255, 255), -1)
        frame = cv2.bitwise_and(frame, mask)
        return frame

    def decanol_push(self, x, y):
        self.head.move(decanol_stock[0], decanol_stock[1])
        vol = 2
        if self.decanol_syringe.canAbsorbVol(vol):
            self.decanol_syringe.syringeSetSpeed(100)
            # self.decanol_syringe.syringeSetAcc(200)
            self.decanol_syringe.syringeMove(-47)
            self.decanol_syringe.plungerPullVol(vol)
            self.decanol_syringe.syringeMove(0)
            self.head.move(decanol_position[0], decanol_position[1])
            # self.decanol_syringe.plungerSetSpeed(150)
            self.decanol_syringe.syringeMove(-46)
            self.decanol_syringe.plungerPushVol(vol)
            self.decanol_syringe.syringeMove(0)
        else:
            print "volume exceeds capacity"
        print x, y

    def salt_push(self, x, y):
        self.head.move(salt_stock[0], salt_stock[1])
        vol = 3
        if self.salt_syringe.canAbsorbVol(vol):
            self.salt_syringe.syringeMove(-55)
            self.salt_syringe.plungerPullVol(vol)
            self.salt_syringe.syringeMove(0)
            self.head.move(salt_position[0], salt_position[1])
            self.salt_syringe.syringeMove(-55)
            self.salt_syringe.plungerPushVol(vol)
            self.salt_syringe.syringeMove(0)
        else:
            print "volume exceeds capacity"
        print x, y

    def add_water(self, water_amount):
        # get water
        self.head.move(water_baker_coord['water'])
        self.decanoate_syringe.syringeMove(-24)
        self.decanoate_syringe.plungerPullVol(water_amount)
        self.decanoate_syringe.syringeMove(0)

        # dispense water
        self.head.move(petridish_center[0], petridish_center[1])
        self.decanoate_syringe.syringeMove(-25)
        # mixing water in petri
        count = 0
        while count < 3:
            self.decanoate_syringe.plungerPushVol(water_amount)
            self.decanoate_syringe.plungerPullVol(water_amount)
            count += 1
        self.decanoate_syringe.plungerMovePos(40)
        self.decanoate_syringe.syringeMove(0)

    def get_decanoate(self, x, y, amount):
        # get decanoate solution
        self.head.move(x, y)
        self.decanoate_syringe.syringeMove(-24)
        self.decanoate_syringe.plungerPullVol(amount)
        self.decanoate_syringe.syringeMove(0)
        print "decanoate poured"
        # dispense decanoate
        self.head.move(petridish_center[0], petridish_center[1])
        print "syringe moved"
        self.decanoate_syringe.syringeMove(-24)
        self.decanoate_syringe.plungerPushVol(amount)
        self.decanoate_syringe.syringeMove(0)

    def prepare_decanoate(self, decanoate_solution):

        ph, molarity = decanoate_solution
        print "pH parameter is " + str(ph) + " and molarity parameter is " + str(molarity)
        final_volume = 9e-3
        # ph_real = 7 + round((13 - 7) * ph)  # give me a number between 7 and 13
        # mol_real = 5 + ((20 - 5) * molarity)  # give me a number between 5 and 20
        # vol_total = 38  # change according to syringe volume
        # vol_decanoate = (0.75 * molarity + 0.25) * vol_total
        # print vol_decanoate
        # vol_water = vol_total - vol_decanoate
        x_sol_ph = math.floor(ph)
        y_sol_ph = x_sol_ph + 1
        x_vol, y_vol = compute_volumes(x_sol_ph, y_sol_ph, molarity, ph)
        h20_volume = final_volume - (x_vol + y_vol)

        x_sol_position = decanoate_bakers_coord[x_sol_ph]
        y_sol_position = decanoate_bakers_coord[y_sol_ph]

        print "adding first solution with pH %f " % x_sol_ph
        self.get_decanoate(x_sol_position[0], x_sol_position[1], x_vol)
        print "adding decanoate"

        print "adding second solution with pH %f " % y_sol_ph
        self.get_decanoate(y_sol_position[0], y_sol_position[1], y_vol)
        print "adding decanoate"

        print "Add water"
        self.add_water(h20_volume)

    def perform_experiment(self):
        global recording
        cv2.namedWindow('Schermata')
        cap = cv2.VideoCapture(CAMERA_ID)
        cap.set(3, 1280)
        cap.set(4, 720)
        ret, frame = cap.read()
        if ret == 0:
            print 'ERROR READING INPUT'

        x, y, z = frame.shape
        mask = np.zeros((x, y, 3), np.uint8)

        start_tracking_time = None

        modality = 'setting'

        while True:
            ret, frame = cap.read()
            if ret == 0:
                print "ERROR READING INPUT"
            result = frame.copy()
            cv2.circle(result, (ix, iy), R, (0, 0, 255), 2)
            # cv2.setMouseCallback('Schermata', self.onClick, param=modality)
            frame = self.mask_frame(frame, mask)

            # OPTIONS
            # Waits up to other 30ms for a keypress
            # If none -> -1
            key = cv2.waitKey(30) & 0xFF

            now = datetime.datetime.now()
            done_tracking = modality == "tracking" and ((start_tracking_time + datetime.timedelta(seconds=60)) <= now)

            if key == 27 or done_tracking:
                print "Stop", done_tracking, start_tracking_time, now
                end_tracking_time = now
                break
            elif key == ord('1'):
                if modality == "setting":
                    print "Already in SETTING MODE. Click to set the center and adjust the radius with the trackbar"
                if modality in ["moving", "tracking"]:
                    modality = "setting"
                    print "SETTING MODE. Click to set the center and adjust the radius with the trackbar"
            elif key == ord('2'):
                if modality == "setting":
                    modality = "moving"
                    print "MOVING MODE. RIGHT Click to push decanol and LEFT Click to push salt"
                    self.decanol_push(decanol_position[0], decanol_position[1])
                    self.salt_push(salt_position[0], salt_position[1])
            elif key == ord('t'):
                if modality in ["setting", "moving"]:
                    self.head.move(0, 0)
                    modality = "tracking"
                    print "TRACKING MODE. C : count droplets"
                    # We start an exp!!!
                    start_tracking_time = datetime.datetime.now()
                if modality == "tracking":
                    # We're already in exp to not reset the timer!!!
                    print "Already in TRACKING MODE. C : count droplets"
            elif key == ord('r'):
                recording = not recording
                if recording:
                    date = str(datetime.datetime.now())
                    import os
                    file_name = os.path.join(FILE_PATH, "%s_video" % vT.removeColon(date))
                    out = cv2.VideoWriter(file_name, fourCC, 10.0, window_size)
                else:
                    out.release()

            # MODALITA'
            track2 = Tracking(frame, decanol_matrix)
            if modality == "setting":
                cv2.putText(result, "SETTING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                cv2.circle(result, (ix, iy), R, (0, 0, 255), 2)
            elif modality == "moving":
                cv2.putText(result, "MOVING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            elif modality == "tracking":
                cv2.putText(result, "TRACKING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                track2.do_track(frame, result)

            if recording:
                out.write(result)
                cv2.putText(result, "RECORDING", (150, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            cv2.imshow('Schermata', result)

        # cv2.waitKey(int(start_tracking_time + datetime.timedelta(seconds=10)))

        _, ending_point = track2.show_output(result)

        if recording:
            import os
            file_name = os.path.join(FILE_PATH, "%s_screen.jpg" % vT.removeColon(date))
            cv2.imwrite(file_name, result)
        cv2.imshow("Schermata", result)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        self.evobot.printcore.disconnect()
        # exit()

        import csv
        with open('/Users/carlottaporcelli/paths/input-{}.csv'.format(start_tracking_time), 'wb') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow([
                'X salt',
                'Y salt',
                'X droplet Start',
                'Y droplet Start',
                'X droplet End',
                'Y droplet End',
                'time',
            ])
            tracking_time = (end_tracking_time - start_tracking_time).total_seconds()

            if salt_position is None or salt_position is None:
                print "maybe you should put on the petri the ingredients"

            writer.writerow([
                salt_position[0],
                salt_position[1],
                decanol_position[0],
                decanol_position[1],
                ending_point[0],
                ending_point[1],
                tracking_time,
            ])
        print('----->', decanol_position, ending_point, salt_position, tracking_time)

        def _calculate_pitagora(a, b):
            return math.sqrt(a ** 2 + b ** 2)

        fitness_value = (_calculate_pitagora(ending_point[0] - salt_position[0], ending_point[1] - salt_position[1])) \
                        / (_calculate_pitagora(decanol_position[0] - ending_point[0],
                                               decanol_position[1] - ending_point[1]))

        print fitness_value
        return fitness_value

    def quit(self):
        self.evobot.disconnect()
        self.head.dataLogger.file.close()
        self.decanoate_syringe.dataLogger.file.close()
