#! /usr/bin/python
from multiDropletTracking import MultiDropletTracking
import numpy as np
import cv2
import VisionTools as vT
import datetime
import time
import os
import sys
sys.path.append('../../settings')
sys.path.append('../../api')

__author__ = "anfv"
__date__ = "$17-Sep-2016 17:39:28$"

if __name__ == "__main__":
    print "Multi Droplet Tracking Example"

# Choose a number between 1 and 7 to track one example
number = 6

# height and width of the video stream
height = 400
width = 400

# Video to open
cap = cv2.VideoCapture('fakeDropletsVideo' + str(number) + '.avi')
# Use cap = cv2.VideoCapture(CAMERA_ID) to use the camera like:
# cap = cv2.VideoCapture(0)

# Petridish center and diameter in pixels
petridishCenter = (200, 200)
petridishDiameter = 240

fourCC = cv2.VideoWriter_fourcc(*'XVID')
window_size = (height, width)

cap.set(3, height)  # Set to the resoluton of the video or camera
cap.set(4, width)  # Set to the resoluton of the video or camera
ret, frame = cap.read()
if ret == 0:
    print 'ERROR READING INPUT'

x, y, z = frame.shape
mask = np.zeros((x, y, 3), np.uint8)

start_tracking_time = None

# Record the video of the identified droplets
recording = True
if recording:
    date = str(datetime.datetime.now())
    file_name = os.path.join("%s_video.avi" % vT.removeColon(date))
    out = cv2.VideoWriter(file_name, fourCC, 10.0, window_size)

start_tracking_time = datetime.datetime.now()

# Init the class MultiDropletTracking
trackObject = MultiDropletTracking()

# Loop to take a frame and track the droplets
while True:
    ret, frame = cap.read()  # Read a new frame
    if ret == 0:
        print "ERROR READING INPUT"
        break

    # Make a copy of the current frame
    result = frame.copy()

    # Draw a circle around the Petri dish object
    cv2.circle(result, petridishCenter, petridishDiameter / 2, (0, 0, 255), 2)

    # Create a mask to only track droplets inside the Petri dish
    mask.fill(0)
    cv2.circle(mask, petridishCenter, petridishDiameter / 2, (255, 255, 255), -1)
    frame = cv2.bitwise_and(frame, mask)

    # Remove all the noise (erosion and dilation)
    kernel = np.ones((2, 2), np.uint8)
    frame = cv2.erode(frame, kernel, iterations=1)
    # frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, kernel)
    # Perform the tracking
    trackObject.do_track(frame, result)

    # Waits up to other 30ms for a keypress
    # If none -> -1
    key = cv2.waitKey(30) & 0xFF

    now = datetime.datetime.now()
    if (start_tracking_time + datetime.timedelta(seconds=40)) <= now:
        # If the time is gone, quit
        print "Stopped"
        end_tracking_time = now
        break
    elif key == ord('q'):
        # If q is pressed, quit
        print "Aborted"
        end_tracking_time = now
        break

    if recording:
        out.write(result)
    cv2.imshow('Multidroplet Tracking', result)
    time.sleep(.2)

# END WHILE TRUE

# Show the results
copyFrame = result.copy()
copyFrame2 = result.copy()
trackObject.show_output(copyFrame)
cv2.imshow('Tracks of all the droplets', copyFrame)

# Show the result of the biggest droplet and give its init and end positions
initPos, endPos = trackObject.show_output_biggest(copyFrame2)
cv2.imshow('Track of the biggest droplet', copyFrame2)
print "init position of the biggest droplet: " + str(initPos)
print "end position of the biggest droplet: " + str(endPos)

# gets all the paths of all the droplets
paths = trackObject.get_paths()

if recording:
    file_name = os.path.join("%s_screen.jpg" % vT.removeColon(date))
    cv2.imwrite(file_name, result)
    out.release()

cv2.waitKey(0)
cv2.destroyAllWindows()
