__author__ = 'Alessandro'

#SISTEMARE IL TRACKING E FAR PROVARE IN LAB

import sys

sys.path.append('../api')
sys.path.append('../settings')

import numpy as np
import cv2
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger
import threading
from myDroplet import MyDroplet
import math

#RED COLOR DEFINITION
lower=np.array([0, 40, 40])
upper=np.array([8, 255, 255])
lower2=np.array([140, 40, 40])
upper2=np.array([180, 255, 255])

# GLOBAL VARIABLES
global M, iM, count, head, syringe, count, frame, R, ix, iy, evobot, droplets

ix = 200
iy = 200
R = 1

dropCount = 0
droplets = []

# SETTINGS
PORT = 'COM4' #Port of evobot

SENSITIVITY_VALUE = 20 #Blurring function parameter
BLUR_SIZE = 10 #Blurring function parameter

MIN_AREA = 10 #Minimum area for droplets to be recognized
MOVEMENT_TOLERANCE = 5 #Limit for updating droplet position
AREA_TOLERANCE = 40 #Limit of area change for updating

DEBUG_MODE = True

def initialize():
    global M, iM, count, head, syringe, frame, evobot, modality
    SYRINGE = {'ID':0, 'SYRINGE_LIMIT':-68, 'PLUNGER_LIMIT':35, 'GOAL_POS':-53} #Set the syringe parameters here
    usrMsgLogger = DataLogger()
    evobot = EvoBot(PORT, usrMsgLogger)
    head = Head(evobot)
    syringe = Syringe(evobot,SYRINGE)
    syringe.plungerSetConversion(1)
    evobot.home()
    syringe.plungerMoveToDefaultPos()
    M = syringe.affineMat
    iM = cv2.invertAffineTransform(M)
    count = 0

def maskFrame(frame, mask):
    global R
    mask.fill(0)
    R = cv2.getTrackbarPos('R','Result')
    cv2.circle(mask, (ix, iy), R, (255, 255, 255), -1)
    frame = cv2.bitwise_and(frame, mask)
    return frame

def nothing(x):
    pass

def onClick(event,x,y,flags,param):
    global ix,iy
    if param == "setting":
        if event == cv2.EVENT_RBUTTONDOWN:
            ix = x
            iy = y
    elif param == "moving":
        if event == cv2.EVENT_RBUTTONDOWN:
            impixel = np.float32([x, y, 1])
            RobCor = np.dot(M, impixel)
            threading.Thread(target=pullAction, args=(RobCor[0], RobCor[1])).start()
        elif event == cv2.EVENT_LBUTTONDOWN:
            impixel = np.float32([x, y, 1])
            RobCor = np.dot(M, impixel)
            threading.Thread(target=pushAction, args=(RobCor[0], RobCor[1])).start()
            #(/*-+)

def pullAction(x, y):
    global syringe, head
    head.move(x, y) #provare a spostare qui per pipeline (/*-+)
    vol = input("Volume to pull [ml] = ")
    if syringe.canAbsorbVol(vol):
        syringe.syringeMove(-50)
        syringe.plungerPullVol(vol)
        syringe.syringeMove(0)
    else:
        print "Volume exceeds capacity"

def pushAction(x, y):
    global syringe, head
    head.move(x, y) #provare a spostare qui per pipeline (/*-+)
    vol = input("Volume to push [ml] = ")
    if syringe.canDispenseVol(vol):
        syringe.syringeMove(-50)
        syringe.plungerPushVol(vol)
        syringe.syringeMove(0)
    else:
        print "Volume exceeds content"

def getSimilarIndex(drop):
    for c in range(0,len(droplets)):
        distance = floatEuclideanDist(droplets[c].centroid, drop.centroid)
        if distance < 50:
            return c
        else:
            continue
    return -1

def isInTheArray(drop): #IMPROVABLE
    for drp in droplets:
        distance = floatEuclideanDist(drp.centroid, drop.centroid)
        if distance < 50:
            return True
    return False

def floatEuclideanDist(p,q):
    px = p[0]
    py = p[1]
    qx = q[0]
    qy = q[1]
    diffX = abs(qx-px)
    diffY = abs(qy-py)
    return float(math.sqrt((diffX * diffX) + (diffY * diffY)))

def track(threshImage, result, color):
    global droplets, dropCount
    _, contours, hierarchy = cv2.findContours(threshImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) #Finds the contours in the image

    if len(contours) == 0:
        pass

    for cnt in contours:
       area = cv2.contourArea(cnt)
       if area >= 1:
            cv2.drawContours(result, cnt, -1, (0, 255, 0), 1)
            MOM = cv2.moments(cnt)
            centroid = (int(MOM['m10']/MOM['m00']), int(MOM['m01']/MOM['m00']))
            drop = MyDroplet(dropCount+1, centroid, cnt, color, area)
            if len(droplets) == 0 or not isInTheArray(drop):
                dropCount += 1
                print "Added droplet " + str(dropCount)
                droplets.append(drop)
            else:
                index = getSimilarIndex(drop)
                if index == -1:
                    print "ERROR"
                    exit()
                droplets[index].updateDroplet(centroid, cnt, area)

def getBiggest():
    max = 0
    for drp in droplets:
        if drp.area > max:
            max = drp.area
            drop = drp
    return drop

def removeBiggest(x, y):
    global syringe, head, modality
    head.move(x, y)
    syringe.syringeMove(-50)
    syringe.plungerPullVol(20) #Adjust for droplets
    syringe.syringeMove(0)
    head.move(0,0)
    modality = "tracking"
    


def main():
    global R, ix, iy, M, modality
    cv2.namedWindow('Result')
    cap = cv2.VideoCapture(1)
    initialize()
    modality = "setting" #it can be "setting", "moving", "tracking"
    cv2.createTrackbar('R', 'Result',0,500,nothing) # Create trackbar for masking
    ret, frame = cap.read()
    if ret == 0:
        print "ERROR READING INPUT"

    x, y, z = frame.shape
    mask = np.zeros((x, y, 3), np.uint8)

    while 1:
        ret, frame = cap.read()
        if ret == 0:
            print "ERROR READING INPUT"

        result = frame.copy()
        cv2.circle(result,(ix,iy),R,(0,0,255),2)
        if modality == "setting":
            cv2.circle(result,(ix,iy),2,(255,0,0),2)
        cv2.setMouseCallback('Result', onClick, modality)
        frame = maskFrame(frame, mask)

        #OPTIONS
        key = cv2.waitKey(10) & 0xFF
        if key == 27:
            exit();
        elif key == ord('s'):
            if modality == "setting":
                modality = "moving"
                print "Area of interest set. R click to pull or L click to push"
            elif modality == "moving":
                modality = "setting"
                print "R click and trackbar to set the area of interest"
        if key == ord('t'):
            if not(modality == "tracking"):
                modality = "tracking"
                threading.Thread(target=head.move(0,0)).start()
                print "Tracking Enabled."
            elif modality == "tracking":
                modality = "moving"
                print "Tracking Disabled. You are in 'moving' mode"

        if (key == ord('z') or key == ord('x')) and not(modality == "tracking"):
            print 'Impossible to remove droplets. Enable tracking first using "t" key'

        if modality == "tracking":
            #Image blurring and thresholding
            frame=cv2.blur(frame, (BLUR_SIZE,BLUR_SIZE))
            frameHSV = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
            threshImage = cv2.bitwise_or(cv2.inRange(frameHSV, lower, upper),cv2.inRange(frameHSV, lower2, upper2))
            threshImage=cv2.blur(threshImage, (BLUR_SIZE,BLUR_SIZE))
            _,threshImage=cv2.threshold(threshImage, SENSITIVITY_VALUE, 255, cv2.THRESH_BINARY)

            if DEBUG_MODE:
                cv2.imshow('Threshold Image', threshImage)

            track(threshImage, result, "red")
            if key == ord('c'):
                 print "there are " + str(dropCount) + " droplets"
            if key == ord('z'):
                print "Removing smallest droplet. Not implemented yet"
            elif key == ord('x'):
                print "Removing biggest droplet"
                modality = "moving"
                drop = getBiggest()
                impixel = np.float32([drop.centroid[0], drop.centroid[1], 1])
                RobCor = np.dot(M, impixel)
                threading.Thread(target=removeBiggest, args=(RobCor[0], RobCor[1])).start()
        cv2.imshow('Result', result)

if __name__ == "__main__":
    main()