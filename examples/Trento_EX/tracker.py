import cv2
import numpy as np
import math
from collections import defaultdict

#RED COLOR DEFINITION
lower=np.array([0, 40, 40])
upper=np.array([8, 255, 255])
lower2=np.array([140, 40, 40])
upper2=np.array([180, 255, 255])

#BLUE COLOR DEFINITION
lower_blue = np.array([60,50,50])
upper_blue = np.array([160,255,255])



SENSITIVITY_VALUE = 20
BLUR_SIZE = 1

MOVEMENT_TOLERANCE = 5
AREA_TOLERANCE = 40

droplets = []
ix = 200
iy = 200
R = 0
paths = defaultdict(list)

counter = 0


class Droplet:
    def __init__(self, dropId, centroid, contour, color, area):
        self.dropId = dropId
        self.centroid = centroid
        self.contour = contour
        self.color = color
        self.area = area
        self.modified = False
        paths[self.dropId].append(centroid)
        
    def updateDroplet(self, centroid, contour, area):
        if floatEuclideanDist(self.centroid,centroid) > MOVEMENT_TOLERANCE:
            self.centroid = centroid
            self.contour = contour
            paths[self.dropId].append(centroid)
            self.modified = True
        if abs(self.area-area) > AREA_TOLERANCE:
            self.area = area
        if self.modified:
            print str(self.dropId) + " has been updated "


def center_coordinate(event,x,y,flags,param):
    global ix,iy
    if event == cv2.EVENT_RBUTTONUP:
        ix=x
        iy = y


def floatEuclideanDist(p,q):
    px = p[0]
    py = p[1]
    qx = q[0]
    qy = q[1]
    diffX = abs(qx-px)
    diffY = abs(qy-py)
    return float(math.sqrt((diffX * diffX) + (diffY * diffY)))


def searchForMovement(thresholdImage,result,color):
    global counter
    global paths
    _, contours, hierarchy = cv2.findContours(thresholdImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    if len(contours) > 0:
        for cnt in contours:
            area = cv2.contourArea(cnt)
            if area>10:
                M = cv2.moments(cnt)
                centroid = (int(M['m10']/M['m00']), int(M['m01']/M['m00']))
                cv2.drawContours(result, cnt, -1, (0, 255, 0), 1)
                circles = cv2.HoughCircles(thresholdImage,cv2.HOUGH_GRADIENT,20,20,param1=50,param2=5)
                if circles is not None:
                    if len(circles) == len(droplets):
                        print "all the droplets are circular   " + str(len(circles))
                if len(droplets) == 0:
                    counter += 1
                    droplets.append(Droplet(counter,centroid,cnt,color,area))
                    print "FIRST INSERT     :    " + str(counter) + "      var centorid  " + str(centroid) + "           drp centroid   " + str(droplets[0].centroid)
                else:
                    for drp in droplets:
                        message = drp.color + "   " + str(drp.dropId) + " - C : " + str(drp.centroid) +"A : " + str(drp.area)
                        cv2.putText(result,message,drp.centroid, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                    distance = floatEuclideanDist(centroid, drp.centroid)
                    if distance > 100 and not drp.modified:
                        counter += 1
                        droplets.append(Droplet(counter, centroid, cnt, color, area))
                    else:
                        if not drp.modified:
                            drp.updateDroplet(centroid, cnt, area)
                    drp.modified = False
    cv2.putText(result, "Tracking ", (20, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)


def nothing(x):
    pass


def maskFrame(frame, mask):
    global R
    mask.fill(0)
    R = cv2.getTrackbarPos('R','Frame 1')
    cv2.circle(mask, (ix, iy), R, (255, 255, 255), -1)
    frame = cv2.bitwise_and(frame, mask)
    return frame


def main():
    global R
    debugMode = False
    trackingEnabled = True
    pause = True
    cap=cv2.VideoCapture(1)
    cv2.namedWindow('Frame 1')
    cv2.createTrackbar('R', 'Frame 1',0,500,nothing)

    ret,frame1 = cap.read()
    #frame1=cv2.imread("prova2.jpg")
    if 1 == 0:
        print "Problems"
        exit()

    x, y, z = frame1.shape
    mask = np.zeros((x, y, 3), np.uint8)
    cv2.setMouseCallback('Frame 1',center_coordinate)
    #---------------------------------------------------------------------------------
    while(1):
        ret,frame1 = cap.read()
        #frame1 = cv2.imread("prova2.jpg")
        if 1 == 0:
            break

        result = frame1.copy()
        frame = frame1.copy()

        cv2.circle(result,(ix,iy),R,(0,0,255),2)

        frame = maskFrame(frame,mask)
        frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        while pause:
            R = cv2.getTrackbarPos('R','Frame 1')
            cv2.circle(result,(ix,iy),2,(255,0,0),3)
            cv2.circle(result,(ix,iy),R,(0,0,255),2)
            cv2.imshow("Frame 1", result)
            result = frame1.copy()
            key=cv2.waitKey(10)
            if key == ord('p'):
                pause = False
                print "Resumed"

        if debugMode:
            cv2.imshow("Final Threshold Image", threshImage)
        else:
            cv2.destroyWindow("Final Threshold Image")
        if trackingEnabled:
            threshImage = cv2.bitwise_or(cv2.inRange(frameHSV, lower, upper),cv2.inRange(frameHSV, lower2, upper2))
            threshImage=cv2.blur(threshImage, (BLUR_SIZE,BLUR_SIZE))
            _,threshImage=cv2.threshold(threshImage, SENSITIVITY_VALUE, 255, cv2.THRESH_BINARY)
            searchForMovement(threshImage, result, "red")
            threshImage = cv2.inRange(frameHSV, lower_blue, upper_blue)
            threshImage=cv2.blur(threshImage, (BLUR_SIZE,BLUR_SIZE))
            _,threshImage=cv2.threshold(threshImage, SENSITIVITY_VALUE, 255, cv2.THRESH_BINARY)
            searchForMovement(threshImage, result, "blue")
        key=cv2.waitKey(10) & 0xFF
        if key == 27:
            break
        elif key == ord('t'):
            trackingEnabled = not trackingEnabled
            if trackingEnabled:
                print "Tracking Enabled"
                #searchForMovement(threshImage, result)
            else:
                print "Tracking Disabled"
        elif key == ord('d'):
            debugMode = not debugMode
            if debugMode:
                print "Debug Mode Enabled"
            else:
                print "Debug Mode Disabled"
        elif key == ord('p') :
            pause = not pause
            print "Paused. Press 'p' to unpause"
        cv2.imshow("Frame 1", result)


    #-------------------------------------------------------------------- DISEGNO
    print "COMPLETE PATH :"
    for drp in droplets:
        for c in paths[drp.dropId]:
            print c

    for drp in droplets:
        for index,c in enumerate(paths[drp.dropId]):
            cv2.circle(result,c,2,(255,0,0),2)
            if index == 0:
                cv2.putText(result,"BEGIN",c,cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            elif index == len(paths[drp.dropId])-1:
                cv2.putText(result,"END",c,cv2.FONT_HERSHEY_SIMPLEX, .5, 255)


    cv2.imshow("Frame 1", result)
    cv2.waitKey(0)


if __name__ == "__main__":
    main()
