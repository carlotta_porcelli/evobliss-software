__author__ = 'Alessandro'

#VERSIONE VECCIA

import sys

sys.path.append('../api')
sys.path.append('../settings')

import numpy as np
import cv2
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger
import threading

#RED COLOR DEFINITION
lower=np.array([0, 40, 40])
upper=np.array([8, 255, 255])
lower2=np.array([140, 40, 40])
upper2=np.array([180, 255, 255])

# GLOBAL VARIABLES
global M, iM, count, head, syringe, count, frame, R, ix, iy, evobot
ix = 200
iy = 200
R = 1

# SETTINGS
PORT = 'COM4' #Port of evobot

SENSITIVITY_VALUE = 20
BLUR_SIZE = 10

MOVEMENT_TOLERANCE = 5 #Limit for updating droplet position
AREA_TOLERANCE = 40 #Limit of area change for updating

DEBUG_MODE = True

def maskFrame(frame, mask):
    global R
    mask.fill(0)
    R = cv2.getTrackbarPos('R','Result')
    cv2.circle(mask, (ix, iy), R, (255, 255, 255), -1)
    frame = cv2.bitwise_and(frame, mask)
    return frame

def nothing(x):
    pass

def onClick(event,x,y,flags,param):
    global ix,iy
    if param == "setting":
        if event == cv2.EVENT_RBUTTONDOWN:
            ix = x
            iy = y
    elif param == "moving":
        if event == cv2.EVENT_RBUTTONDOWN:
            impixel = np.float32([x, y, 1])
            RobCor = np.dot(M, impixel)
            threading.Thread(target=pullAction, args=(RobCor[0], RobCor[1])).start()
        elif event == cv2.EVENT_LBUTTONDOWN:
            impixel = np.float32([x, y, 1])
            RobCor = np.dot(M, impixel)
            threading.Thread(target=pushAction, args=(RobCor[0], RobCor[1])).start()
            #(/*-+)

def pullAction(x, y):
    global syringe, head
    head.move(x, y) #provare a spostare qui per pipeline (/*-+)
    vol = input("Volume to pull [ml] = ")
    if syringe.canAbsorbVol(vol):
        syringe.syringeMove(-50)
        syringe.plungerPullVol(vol)
        syringe.syringeMove(0)
    else:
        print "Volume exceeds capacity"

def pushAction(x, y):
    global syringe, head
    head.move(x, y) #provare a spostare qui per pipeline (/*-+)
    vol = input("Volume to push [ml] = ")
    if syringe.canDispenseVol(vol):
        syringe.syringeMove(-50)
        syringe.plungerPushVol(vol)
        syringe.syringeMove(0)
    else:
        print "Volume exceeds content"

def track():
    print "I'm identifying"

def initialize():
    global M, iM, count, head, syringe, frame, evobot
    SYRINGE = {'ID':0, 'SYRINGE_LIMIT':-68, 'PLUNGER_LIMIT':35, 'GOAL_POS':-53} #Set the syringe parameters here
    usrMsgLogger = DataLogger()
    evobot = EvoBot(PORT, usrMsgLogger)
    head = Head(evobot)
    syringe = Syringe(evobot,SYRINGE)
    syringe.plungerSetConversion(1)
    evobot.home()
    syringe.plungerMoveToDefaultPos()
    M = syringe.affineMat
    iM = cv2.invertAffineTransform(M)
    count = 0

def main():
    global R, ix, iy
    cv2.namedWindow('Result')
    cap = cv2.VideoCapture(1)
    initialize()
    modality = "setting" #it can be "setting", "moving", "tracking"
    cv2.createTrackbar('R', 'Result',0,500,nothing) # Create trackbar for masking
    ret, frame = cap.read()
    if ret == 0:
        print "ERROR READING INPUT"

    x, y, z = frame.shape
    mask = np.zeros((x, y, 3), np.uint8)

    while 1:
        ret, frame = cap.read()
        if ret == 0:
            print "ERROR READING INPUT"

        result = frame.copy()
        cv2.circle(result,(ix,iy),R,(0,0,255),2)
        if modality == "setting":
            cv2.circle(result,(ix,iy),2,(255,0,0),2)
        cv2.setMouseCallback('Result', onClick, modality)
        frame = maskFrame(frame, mask)

        #OPTIONS
        key = cv2.waitKey(10) & 0xFF
        if key == 27:
            exit();
        elif key == ord('s'):
            if modality == "setting":
                modality = "moving"
                print "Area of interest set. R click to pull or L click to push"
            elif modality == "moving":
                modality = "setting"
                print "R click and trackbar to set the area of interest"
        if key == ord('t'):
            if not(modality == "tracking"):
                modality = "tracking"
                print "Tracking Enabled."
            elif modality == "tracking":
                modality = "moving"
                print "Tracking Disabled. You are in 'moving' mode"

        if (key == ord('z') or key == ord('x')) and not(modality == "tracking"):
            print 'Impossible to remove droplets. Enable tracking first using "t" key'

        if modality == "tracking":
            track()
            if key == ord('z'):
                print "Removing smallest droplet"
            elif key == ord('x'):
                print "Removing biggest droplet"
        cv2.imshow('Result', result)

if __name__ == "__main__":
    main()