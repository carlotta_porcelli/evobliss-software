import math
import sys
import traceback
import numpy as np

# 9.3 -> 9.12 mol 15
# 10.8 -> 10.72 mol 15
# 10.8 ->  10.10    mol 20
# 8.4 -> 8.1 mol 10 h2o 5.3
# 8.4 -> 8.3 mol 10 h2o 7
# 7 -> 6.8 mol 10 h2o 6

# final_molarity = 13e-3

h2o_ph = 6

min_ph = 7
max_ph = 13

# list of all possible phs
ph_values = np.arange(7, 12.3, 0.001)
list_of_phs = []
for el in ph_values:
    list_of_phs.append(float('%.2f' % el))

# list of all possible molarities
mol_values = np.arange(5, 20, 0.001)
list_of_mols = []
for mol in mol_values:
    list_of_mols.append(float('%.2f' % mol))
print list_of_mols

# calculations withouth water
# b = (x_oh_conc * final_molarity * final_volume ) / x_molarity
# a = final_oh_conc * final_volume
# y_volume = (a-b)/(y_oh_conc-x_oh_conc)
# x_volume = ((final_molarity*final_volume) - (y_volume * y_molarity)) / x_molarity

# calculations with water
x_molarity = 20e-3
y_molarity = x_molarity
final_volume = 9e-3


def compute_volumes(x_ph, y_ph, f_mol):
    final_oh_conc = 1.0 / (10 ** (14 - final_ph))
    x_oh_conc = 1.0 / (10 ** (14 - x_ph))
    y_oh_conc = 1.0 / (10 ** (14 - y_ph))
    h2o_oh_conc = 1.0 / (10 ** (14 - h2o_ph))
    y_vol = (final_oh_conc * final_volume + (f_mol * final_volume / x_molarity) * h2o_oh_conc -
             (f_mol * final_volume / x_molarity) * x_oh_conc - final_volume * h2o_oh_conc) / \
            (y_oh_conc - x_oh_conc)
    x_vol = ((f_mol * final_volume) / x_molarity) - y_vol
    if x_vol < 0:
        if y_ph < 13:
            x_vol, y_vol = compute_volumes(x_ph, y_ph + 1, f_mol)
    elif y_vol < 0:
        if x_ph > 7:
            x_vol, y_vol = compute_volumes(x_ph - 1, y_ph, f_mol)
    return x_vol, y_vol

for final_mol in list_of_mols:
    print 'final molarity is: ' + str(final_mol)
    for final_ph in list_of_phs:
        # print 'final ph and final molarity are: ' + str(final_ph) + ' ' + str(final_mol)
        x_sol_ph = math.floor(final_ph)
        y_sol_ph = x_sol_ph + 1
        f = final_mol*10**(-3)  # the values in the list are not in mM
        # if 7<= x_sol_ph < 13 and 7 <= y_sol_ph < 13
        x_volume, y_volume = compute_volumes(x_sol_ph, y_sol_ph, f)
        h20_volume = final_volume - (x_volume + y_volume)
        if x_volume < 0 or y_volume < 0:
            print 'ph x: ' + str(x_sol_ph)
            print 'ph y: ' + str(y_sol_ph)
            print "volume of solution x: " + str(x_volume * 1000)
            print "volume of solution y: " + str(y_volume * 1000)
            traceback.print_exc(file=sys.stdout)
            sys.exit(-1)
        # print "volume of solution water: " + str(h20_volume * 1000)
        # print "volume final : " + str(y_volume * 1000 + x_volume * 1000 + h20_volume * 1000)

print "Finished the comprobation without errors (no negative numbers)" 

