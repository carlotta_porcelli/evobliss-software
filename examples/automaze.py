import sys

sys.path.append('../api')
sys.path.append('../settings')

from settings import CAMERA_ID, PORT_NO, FILE_PATH
from configuration import *

import numpy as np
import cv2
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger

import datetime
import VisionTools as vT
from tracking import Tracking

# GLOBAL VARIABLES
global frame, evobot, decanol_matrix, decanol_matrix_transformed, salt_matrix, salt_matrix_transformed, ix, iy,\
    syringes

ix = 40
iy = 60
R = 1

other_start_position = None
salt_position = (75, 193)
decanol_position = (95, 193)

petri_dish_center = (70, 70)

date = ""
fourCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
window_size = (1280, 720)

recording = False

start_tracking_time = None
end_tracking_time = None


def maskFrame(frame, mask):
    global R
    mask.fill(0)
    R = cv2.getTrackbarPos('R', 'Schermata')
    cv2.circle(mask, (ix, iy), R, (255, 255, 255), -1)
    frame = cv2.bitwise_and(frame, mask)
    return frame


def nothing():
    pass


def trasform_matrixes():
    global decanol_matrix, decanol_matrix_transformed, salt_matrix, salt_matrix_transformed
    decanol_matrix = decanol_syringe.affineMat
    decanol_matrix_transformed = cv2.invertAffineTransform(decanol_matrix)
    salt_matrix = salt_syringe.affineMat
    salt_matrix_transformed = cv2.invertAffineTransform(salt_matrix)


def onClick(event, x, y, flags, param):
    global ix, iy
    if param == "setting":
        if cv2.EVENT_RBUTTONUP == event:
            ix = x
            iy = y
            print "queste sono le coordinate del cerchio rosso: " + str(ix) + str(iy)
    elif param == "moving":
        if event == cv2.EVENT_RBUTTONUP:  # tasto destro per mettere giu il decanolo
            impixel = np.float32([x, y, 1])
            rob_cor = np.dot(decanol_matrix, impixel)
            decanol_push(rob_cor[0], rob_cor[1])
        elif event == cv2.EVENT_LBUTTONDOWN:  # tasto sinistro per mettere giu il sale
            impixel = np.float32([x, y, 1])
            rob_cor = np.dot(salt_matrix, impixel)
            salt_push(rob_cor[0], rob_cor[1])


def decanol_push(x, y):
    head.move(x, y)
    vol = 2
    if decanol_syringe.canAbsorbVol(vol):
        decanol_syringe.syringeSetSpeed(100)
        # decanol_syringe.plungerSetSpeed(288)
        decanol_syringe.syringeMove(-47)
        decanol_syringe.plungerPullVol(vol)
        decanol_syringe.syringeMove(0)
        head.move(decanol_position[0], decanol_position[1])
        # decanol_syringe.syringeSetSpeed(300)
        decanol_syringe.plungerSetSpeed(100)
        decanol_syringe.syringeMove(-46)
        # decanol_syringe.plungerSetSpeed(130)
        decanol_syringe.plungerPushVol(vol)
        decanol_syringe.syringeMove(0)
    else:
        print "volume exceeds capacity"
    print x, y


def salt_push(x, y):
    head.move(x, y)
    vol = 2
    if salt_syringe.canAbsorbVol(vol):
        salt_syringe.syringeMove(-60)
        salt_syringe.plungerPullVol(vol)
        salt_syringe.syringeMove(0)
        head.move(salt_position[0], salt_position[1])
        salt_syringe.syringeMove(-60)
        salt_syringe.plungerPushVol(vol)
        salt_syringe.syringeMove(0)
    else:
        print "volume exceeds capacity"
    print x, y


def initialize():
    global head, sockets, decanol_syringe, salt_syringe, evobot, syringes
    syringes = []
    usrmsglogger = DataLogger()
    evobot = EvoBot(PORT_NO, usrmsglogger)
    head = Head(evobot)
    head.setSpeed(2000)

    sockets = evobot.getPopulatedSockets()
    decanol_syringe = Syringe(evobot, SYRINGE2)
    salt_syringe = Syringe(evobot, SYRINGE1)

    for c in sockets:
        syr = Syringe(evobot, {'ID': int(c), 'SYRINGE_LIMIT': -68, 'PLUNGER_LIMIT': 35, 'GOAL_POS': -55,
                               'PLUNGER_CONVERSION_FACTOR': 1})
        syr.plungerMovePos(30)
        # syr.home()
        syr.syringeMove(0)
        syringes.append(syr)

    head.home()
    trasform_matrixes()


def main():
    global R, ix, iy, modality, window_size, evobot, sockets, recording, date, track2, start_tracking_time, end_tracking_time
    cv2.namedWindow('Schermata')
    cap = cv2.VideoCapture(CAMERA_ID)
    cap.set(3, 1280)
    cap.set(4, 720)
    # cap.set(3, window_size[0])
    # cap.set(4, window_size[1])
    initialize()
    sockets = evobot.getPopulatedSockets()

    modality = 'setting'

    cv2.createTrackbar('R', 'Schermata', 0, 500, nothing)  # Create trackbar for R circle

    ret, frame = cap.read()
    if ret == 0:
        print 'ERROR READING INPUT'

    x, y, z = frame.shape
    mask = np.zeros((x, y, 3), np.uint8)

    start_tracking_time = None

    while True:
        ret, frame = cap.read()
        if ret == 0:
            print "ERROR READING INPUT"

        result = frame.copy()
        cv2.circle(result, (ix, iy), R, (0, 0, 255), 2)
        cv2.setMouseCallback('Schermata', onClick, param=modality)
        frame = maskFrame(frame, mask)

        # OPTIONS
        # Waits up to other 30ms for a keypress
        # If none -> -1
        key = cv2.waitKey(30) & 0xFF

        now = datetime.datetime.now()
        done_tracking = modality == "tracking" and ((start_tracking_time + datetime.timedelta(seconds=60)) <= now)

        if key == 27 or done_tracking:
            print "Stop", done_tracking, start_tracking_time, now
            end_tracking_time = now
            break
        elif key == ord('1'):
            if modality == "setting":
                print "Already in SETTING MODE. Click to set the center and adjust the radius with the trackbar"
            if modality == "moving":
                modality = "setting"
                print "SETTING MODE. Click to set the center and adjust the radius with the trackbar"
            if modality == "tracking":
                modality = "setting"
                print "SETTING MODE. Click to set the center and adjust the radius with the trackbar"
        elif key == ord('2'):
            if modality == "setting":
                modality = "moving"
                print "MOVING MODE. RIGHT Click to push decanol and LEFT Click to push salt"
            if modality == "moving":
                print "Already in MOVING MODE. RIGHT Click to pull and LEFT Click to push"
            if modality == "tracking":
                modality = "moving"
                print "MOVING MODE. RIGHT Click to pull and LEFT Click to push"
        elif key == ord('t'):
            if modality in ["setting", "moving"]:
                head.move(0, 0)
                modality = "tracking"
                print "TRACKING MODE. C : count droplets"
                # We start an exp!!!
                start_tracking_time = datetime.datetime.now()

            if modality == "tracking":
                # We're already in exp to not reset the timer!!!
                print "Already in TRACKING MODE. C : count droplets"

        elif key == ord('r'):
            recording = not recording
            if recording:
                date = str(datetime.datetime.now())
                import os
                file_name = os.path.join(FILE_PATH, "%s_video" % vT.removeColon(date))
                out = cv2.VideoWriter(file_name, fourCC, 10.0, window_size)
            else:
                out.release()

        # MODALITA'
        track2 = Tracking(frame, decanol_matrix)
        if modality == "setting":
            cv2.putText(result, "SETTING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            cv2.circle(result, (ix, iy), 1, (0, 0, 255), 2)
        elif modality == "moving":
            cv2.putText(result, "MOVING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
        elif modality == "tracking":
            cv2.putText(result, "TRACKING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            track2.do_track(frame, result)

        if recording:
            out.write(result)
            cv2.putText(result, "RECORDING", (150, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
        cv2.imshow('Schermata', result)

    # cv2.waitKey(int(start_tracking_time + datetime.timedelta(seconds=10)))

    _, ending_point = track2.show_output(result)

    import csv
    with open('/Users/carlottaporcelli/paths/input-{}.csv'.format(start_tracking_time), 'wb') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow([
            'X salt',
            'Y salt',
            'X droplet Start',
            'Y droplet Start',
            'X droplet End',
            'Y droplet End',
            'time',
        ])
        tracking_time = (end_tracking_time - start_tracking_time).total_seconds()

        if salt_position is None or salt_position is None:
            print "maybe you should put on the petri the ingredients"

        writer.writerow([
            salt_position[0],
            salt_position[1],
            decanol_position[0],
            decanol_position[1],
            ending_point[0],
            ending_point[1],
            tracking_time,
        ])

    print('----->', decanol_position, ending_point, salt_position, tracking_time)

    if recording:
        import os
        file_name = os.path.join(FILE_PATH, "%s_screen.jpg" % vT.removeColon(date))
        cv2.imwrite(file_name, result)
    cv2.imshow("Schermata", result)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    evobot.printcore.disconnect()
    exit()


if __name__ == "__main__":
    main()
