import time
import sys

sys.path.append('../api')
from evocam import EvoCam

evocam=EvoCam( [[140,40,40],[180,255,255]],1300,1600, 1, 'camera-calibration.npy')

evocam.saveVideo( 'test' )
evocam.windowOpen()

while True:
    try:
        [x,y] = evocam.getDropletPos()
        print x, y
        evocam.windowUpdate()
        #time.sleep(0.5)
    except KeyboardInterrupt:
        break;
        pass

evocam.disconnect()

