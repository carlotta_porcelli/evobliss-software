import time
import sys
sys.path.append('../api')
sys.path.append('../settings')
from configuration import *
from evobot import EvoBot
from datalogger import DataLogger
from syringe import Syringe
from pump import Pump
from head import Head


usrMsgLogger = DataLogger()
evobot = EvoBot(PORT_NO, usrMsgLogger)
head = Head( evobot )
pump =  Pump( evobot, PUMPS['PUMP1'])

pump.setSpeed(90)
pump.pumpPullVol( 210 )
pump.pumpPushVol(5)
evobot.disconnect()
