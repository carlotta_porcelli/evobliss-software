import time
import sys

sys.path.append('../api')
from evobot import EvoBot
from evocam import EvoCam
from datalogger import DataLogger

usrMsgLogger = DataLogger()
evobot = EvoBot("/dev/tty.usbmodem1421", usrMsgLogger)  # this logs messages intended for the human user by printing them to terminal
headDataLogger = DataLogger('head-postion.dat')
evobot.head.setDataLogger(headDataLogger)  # this logs head positions to the named file

evocam = EvoCam([[140, 40, 40], [180, 255, 255]], 1300, 1600, 1, 'camera-calibration.npy')
evocam.setDataLogger(DataLogger('droplet-postion.dat'))
evocam.windowOpen()

while True:
    try:
        [x, y] = evocam.getDropletPos()
        evocam.windowUpdate()
        evobot.head.move(x, y)
        time.sleep(0.1)
    except KeyboardInterrupt:
        break
        pass

evobot.disconnect()
evocam.disconnect()
