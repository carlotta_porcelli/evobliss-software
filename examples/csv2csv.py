import re
import csv
import math
import datetime
import glob


class Point(object):
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def get_printable_cell(self):
        return '{}:{}'.format(self.x, self.y)


class Experiment(object):
    def __init__(self, time, salt, start, end):
        self.time = int(float(time) / 1000)
        self.salt = salt
        self.start = start
        self.end = end

    def _calculate_pitagora(self, a, b):
        return math.sqrt(a ** 2 + b ** 2)

    def _super_tia_formula(self):
        return (
            self._calculate_pitagora(self.end.x - self.salt.x, self.end.y - self.salt.y)
        ) / (
            self._calculate_pitagora(self.start.x - self.end.x, self.start.y - self.end.y)
        )

    def _super_magic_formula(self):
        return (
            self._calculate_pitagora(self.start.x - self.salt.x, self.start.y - self.salt.y) / self.time
        ) / (
            self._calculate_pitagora(self.end.x - self.salt.x, self.end.y - self.salt.y)
        )

    def get_printable_row(self, file_name):
        return [
            re.search('pH(\d*)\-', file_name).group(1),
            re.search('\-(\d*)mM', file_name).group(1),
            re.search('(\d)ph11-10mM-d\.csv', file_name).group(1),
            str(self._super_tia_formula()).replace('.', ','),
            str(self._super_magic_formula()).replace('.', ','),
            self.time,
        ]


if __name__ == "__main__":
    to_write = []
    with open('output.csv', 'wb') as fout:
        writer = csv.writer(fout, delimiter=';')
        writer.writerow(['ph', 'millimolare', 'prova', 'tia', 'result: 1/s', 'time'])

        for file_ in glob.glob('input-*'):
            with open(file_, 'rb') as fin:
                reader = csv.DictReader(fin, delimiter=';')

                for data in reader:
                    salt = Point(data['X salt'], data['Y salt'])
                    start = Point(data['X droplet Start'], data['Y droplet Start'])
                    end = Point(data['X droplet End'], data['Y droplet End'])

                    to_write.append(
                        Experiment(
                            data['time'], salt, start, end
                        ).get_printable_row(file_)
                    )

        for line in sorted(to_write, key=lambda l: l[0] + l[1] + l[2]):
            writer.writerow(line)
