import time
import sys
sys.path.append('../api')
sys.path.append('../settings')
from configuration import *
from evobot import EvoBot
from datalogger import DataLogger
from syringe import Syringe
from head import Head

usrMsgLogger = DataLogger()
evobot = EvoBot(PORT_NO, usrMsgLogger)
head = Head( evobot )
syringe =  Syringe( evobot, SYRINGE0)
evobot.home()

syringe.plungerMoveToDefaultPos()
        
        #Setting the maximum speed 
#syringe.plungerSetSpeed(288)
#syringe.plungerSetAcc(96)
        
        #Setting the minimum speed 
syringe.plungerSetSpeed(12)
syringe.plungerSetAcc(12)
syringe.syringeMove( -30 )
syringe.plungerPullVol( 5 ) 
syringe.plungerPushVol(5)
syringe.syringeMove( 0 )

evobot.disconnect()
