import sys

sys.path.append('../api')
sys.path.append('../settings')

from settings import CAMERA_ID, PORT_NO, FILE_PATH
from configuration import *

import numpy as np
import cv2
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger

import datetime
import VisionTools as vT
from tracking import Tracking

salt_position = (75, 193)  # salt position on petridish
decanol_position = (95, 193)  # decanol position on petridish
salt_stock = (120, 0)  # position of salt well on experimental space
decanol_stock = (120, 0)  # position of decanol well on experimental space
decanoate_positions = [[140, 60], [140, 70], [140, 80], [140, 90],
                       [140, 100], [140, 110], [140, 120]]  # posizioni ph dal 7 al 13
water_position = (100, 100)

petridish_center = (100, 193)

"""video recording variables"""
date = ""
fourCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
recording = False

start_tracking_time = None
end_tracking_time = None
window_size = (1280, 720)

"""variables for red circle used to delineate the area of the experiment"""
ix = 40
iy = 60
R = 150


def decanol_push(x, y):
    head.move(decanol_stock[0], decanol_stock[1])
    vol = 2
    if decanol_syringe.canAbsorbVol(vol):
        decanol_syringe.syringeSetSpeed(100)
        decanol_syringe.syringeMove(-47)
        decanol_syringe.plungerPullVol(vol)
        decanol_syringe.syringeMove(0)
        head.move(decanol_position[0], decanol_position[1])
        decanol_syringe.plungerSetSpeed(100)
        decanol_syringe.syringeMove(-46)
        decanol_syringe.plungerPushVol(vol)
        decanol_syringe.syringeMove(0)
    else:
        print "volume exceeds capacity"
    print x, y


def salt_push(x, y):
    head.move(salt_stock[0], salt_stock[1])
    vol = 2
    if salt_syringe.canAbsorbVol(vol):
        salt_syringe.syringeMove(-60)
        salt_syringe.plungerPullVol(vol)
        salt_syringe.syringeMove(0)
        head.move(salt_position[0], salt_position[1])
        salt_syringe.syringeMove(-60)
        salt_syringe.plungerPushVol(vol)
        salt_syringe.syringeMove(0)
    else:
        print "volume exceeds capacity"
    print x, y


def trasform_matrixes():
    global decanol_matrix, decanol_matrix_transformed, salt_matrix, salt_matrix_transformed
    decanol_matrix = decanol_syringe.affineMat
    decanol_matrix_transformed = cv2.invertAffineTransform(decanol_matrix)
    salt_matrix = salt_syringe.affineMat
    salt_matrix_transformed = cv2.invertAffineTransform(salt_matrix)


class DropletTracker:
    def __init__(self):
        self.main()

    @staticmethod
    def maskFrame(frame, mask):
        global R
        mask.fill(0)
        # R = cv2.getTrackbarPos('R', 'Schermata')
        cv2.circle(mask, (petridish_center[0], petridish_center[1]), R, (255, 255, 255), -1)
        frame = cv2.bitwise_and(frame, mask)
        return frame

    def nothing(self, *args, **kvargs):
        pass

    """
    flags parameter to be added as compulsory param for the 'onMouse' funct
    """
    def onClick(self, event, x, y, flags, param):
        global ix, iy
        if param == "setting":
            if cv2.EVENT_RBUTTONUP == event:
                ix = x
                iy = y
        elif param == "moving":
            if event == cv2.EVENT_RBUTTONUP:  # tasto destro per mettere giu il decanolo
                impixel = np.float32([x, y, 1])
                rob_cor = np.dot(decanol_matrix, impixel)
                decanol_push(rob_cor[0], rob_cor[1])
            elif event == cv2.EVENT_LBUTTONDOWN:  # tasto sinistro per mettere giu il sale
                impixel = np.float32([x, y, 1])
                rob_cor = np.dot(salt_matrix, impixel)
                salt_push(rob_cor[0], rob_cor[1])

    @staticmethod
    def initialize():
        global head, sockets, decanol_syringe, salt_syringe, evobot, syringes, decanoate_syringe
        syringes = []
        usrmsglogger = DataLogger()
        evobot = EvoBot(PORT_NO, usrmsglogger)
        head = Head(evobot)
        head.setSpeed(2000)

        sockets = evobot.getPopulatedSockets()
        decanol_syringe = Syringe(evobot, SYRINGE0)
        salt_syringe = Syringe(evobot, SYRINGE1)
        decanoate_syringe = Syringe(evobot, SYRINGE2)

        for c in sockets:
            syr = Syringe(evobot, {'ID': int(c), 'SYRINGE_LIMIT': -68, 'PLUNGER_LIMIT': 35, 'GOAL_POS': -55,
                                   'PLUNGER_CONVERSION_FACTOR': 1})
            syr.plungerMovePos(30)
            syr.syringeMove(0)
            syringes.append(syr)

        head.home()
        trasform_matrixes()

    def addWater(self, water_amount):
        # get water
        self.head.move(water_position[0], water_position[1])
        self.decanoate_syringe.syringeMove(-50)
        self.decanoate_syringe.plungerPullVol(water_amount)
        self.decanoate_syringe.syringeMove(0)

        # dispense water
        self.head.move(petridish_center)
        self.decanoate_syringe.syringeMove(-40)
        self.decanoate_syringe.plungerPushVol(water_amount)
        self.decanoate_syringe
        self.decanoate_syringe.syringeMove(0)

    def getDecanoate(self, x, y, amount):
        # get decanoate solution
        self.head.move(x, y)
        self.decanoate_syringe.syringeMove(-40)
        self.decanoate_syringe.plungerPullVol(amount)
        self.decanoate_syringe.syringeMove(0)

        # dispense decanoate
        self.head.move(petridish_center)
        self.decanoate_syringe.syringeMove(-40)
        self.decanoate_syringe.plungerPushVol(amount)
        self.decanoate_syringe.syringeMove(0)

    def prepare_decanoate(self, decanoate_solution):
        ph, molarity = decanoate_solution
        ph_real = 7 + round((13 - 7) * ph)  # give me a number between 7 and 13
        mol_real = 5 + ((20 - 5) * molarity)
        vol_total = 9  # TODO change according to syringe volume
        vol_decanoate = molarity * vol_total
        vol_water = vol_total * (1 - molarity)

        ph_real = {
            7: self.getDecanoate(decanoate_positions[0], vol_decanoate),
            8: self.getDecanoate(decanoate_positions[1], vol_decanoate),
            9: self.getDecanoate(decanoate_positions[2], vol_decanoate),
            10: self.getDecanoate(decanoate_positions[3], vol_decanoate),
            11: self.getDecanoate(decanoate_positions[4], vol_decanoate),
            12: self.getDecanoate(decanoate_positions[5], vol_decanoate),
            13: self.getDecanoate(decanoate_positions[6], vol_decanoate)
        }
        ph_real[7]

        mol_real = {
            5: self.addWater(vol_water),
            5.5: self.addWater(vol_water),
            6: self.addWater(vol_water),
            6.5: self.addWater(vol_water),
            7: self.addWater(vol_water),
            7.5: self.addWater(vol_water),
            8: self.addWater(vol_water),
            8.5: self.addWater(vol_water),
            9: self.addWater(vol_water),
            9.5: self.addWater(vol_water),
            10: self.addWater(vol_water),
            10.5: self.addWater(vol_water),
            11: self.addWater(vol_water),
            11.5: self.addWater(vol_water),
            12: self.addWater(vol_water),
            12.5: self.addWater(vol_water),
            13: self.addWater(vol_water),
            13.5: self.addWater(vol_water),
            14: self.addWater(vol_water),
            14.5: self.addWater(vol_water),
            15: self.addWater(vol_water),
            15.5: self.addWater(vol_water),
            16: self.addWater(vol_water),
            16.5: self.addWater(vol_water),
            17: self.addWater(vol_water),
            17.5: self.addWater(vol_water),
            18: self.addWater(vol_water),
            18.5: self.addWater(vol_water),
            19: self.addWater(vol_water),
            19.5: self.addWater(vol_water),
            20: self.addWater(vol_water)
        }
        mol_real[5]

    @staticmethod
    def perform_experiment():
        decanol_push(decanol_stock[0], decanol_stock[1])
        salt_push(salt_stock[0], salt_stock[1])

    def main(self):
        global R, ix, iy, modality, window_size, evobot, sockets, recording, date, track2, start_tracking_time, end_tracking_time
        cv2.namedWindow('Schermata')
        cap = cv2.VideoCapture(CAMERA_ID)
        cap.set(3, 1280)
        cap.set(4, 720)
        self.initialize()
        sockets = evobot.getPopulatedSockets()

        modality = 'setting'

        # cv2.createTrackbar('R', 'Schermata', 0, 500, self.nothing)  # Create trackbar for R circle

        ret, frame = cap.read()
        if ret == 0:
            print 'ERROR READING INPUT'

        x, y, z = frame.shape
        mask = np.zeros((x, y, 3), np.uint8)

        start_tracking_time = None

        while True:
            ret, frame = cap.read()
            if ret == 0:
                print "ERROR READING INPUT"
            result = frame.copy()
            cv2.circle(result, (petridish_center[0], petridish_center[1]), R, (0, 0, 255), 2)
            cv2.setMouseCallback('Schermata', self.onClick, param=modality)
            frame = self.maskFrame(frame, mask)

            # OPTIONS
            # Waits up to other 30ms for a keypress
            # If none -> -1
            key = cv2.waitKey(30) & 0xFF

            now = datetime.datetime.now()
            done_tracking = modality == "tracking" and ((start_tracking_time + datetime.timedelta(seconds=60)) <= now)

            if key == 27 or done_tracking:
                print "Stop", done_tracking, start_tracking_time, now
                end_tracking_time = now
                break
            elif key == ord('1'):
                if modality == "setting":
                    print "Already in SETTING MODE. Click to set the center and adjust the radius with the trackbar"
                if modality in ["moving", "tracking"]:
                    modality = "setting"
                    print "SETTING MODE. Click to set the center and adjust the radius with the trackbar"
            elif key == ord('2'):
                if modality == "setting":
                    modality = "moving"
                    print "MOVING MODE. RIGHT Click to push decanol and LEFT Click to push salt"
                    self.perform_experiment()
            elif key == ord('t'):
                if modality in ["setting", "moving"]:
                    head.move(0, 0)
                    modality = "tracking"
                    print "TRACKING MODE. C : count droplets"
                    # We start an exp!!!
                    start_tracking_time = datetime.datetime.now()
                if modality == "tracking":
                    # We're already in exp to not reset the timer!!!
                    print "Already in TRACKING MODE. C : count droplets"
            elif key == ord('r'):
                recording = not recording
                if recording:
                    date = str(datetime.datetime.now())
                    import os
                    file_name = os.path.join(FILE_PATH, "%s_video" % vT.removeColon(date))
                    out = cv2.VideoWriter(file_name, fourCC, 10.0, window_size)
                else:
                    out.release()

            # MODALITA'
            track2 = Tracking(frame, decanol_matrix)
            if modality == "setting":
                cv2.putText(result, "SETTING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                cv2.circle(result, (petridish_center[0], petridish_center[1]), R, (0, 0, 255), 2)
            elif modality == "moving":
                cv2.putText(result, "MOVING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            elif modality == "tracking":
                cv2.putText(result, "TRACKING MODE", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                track2.do_track(frame, result)

            if recording:
                out.write(result)
                cv2.putText(result, "RECORDING", (150, 20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            cv2.imshow('Schermata', result)

        # cv2.waitKey(int(start_tracking_time + datetime.timedelta(seconds=10)))

        _, ending_point = track2.show_output(result)

        import csv
        with open('/Users/carlottaporcelli/paths/input-{}.csv'.format(start_tracking_time), 'wb') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow([
                'X salt',
                'Y salt',
                'X droplet Start',
                'Y droplet Start',
                'X droplet End',
                'Y droplet End',
                'time',
            ])
            tracking_time = (end_tracking_time - start_tracking_time).total_seconds()

            if salt_position is None or salt_position is None:
                print "maybe you should put on the petri the ingredients"

            writer.writerow([
                salt_position[0],
                salt_position[1],
                decanol_position[0],
                decanol_position[1],
                ending_point[0],
                ending_point[1],
                tracking_time,
            ])

        print('----->', decanol_position, ending_point, salt_position, tracking_time)

        if recording:
            import os
            file_name = os.path.join(FILE_PATH, "%s_screen.jpg" % vT.removeColon(date))
            cv2.imwrite(file_name, result)
        cv2.imshow("Schermata", result)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        evobot.printcore.disconnect()
        exit()

