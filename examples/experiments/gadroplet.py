import random
from dropletTracker import DropletTracker
import sys
from deap import base
from deap import creator
from deap import tools

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

IND_SIZE = 4

toolbox = base.Toolbox()
toolbox.register("attribute", random.random)

toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attibute, n=IND_SIZE)

toolbox.register("population", tools.initRepeat, list, toolbox.individual)


def evalMin(individual):
    dropletTracking = DropletTracker()
    return True


toolbox.register("evaluate", evalMin)

toolbox.register("mate", tools.cxTwoPoint)

toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)

toolbox.register("select", tools.selBest, 4)


