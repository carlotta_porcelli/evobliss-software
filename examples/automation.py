import sys
sys.path.append('../api')
sys.path.append('../settings')
from settings import CAMERA_ID, PORT_NO
from configuration import *
import cv2
from evobot import EvoBot
from syringe import Syringe
from head import Head
from datalogger import DataLogger


def fai_cose(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONUP:
        ix = x
        iy = y
        head.move(40, 60)
        decanol_syringe.syringeMove(-30)
        decanol_syringe.plungerMovePos(20)
        decanol_syringe.syringeMove(0)
        head.home()


def initialize():
    global head, sockets, decanol_syringe, salt_syringe, evobot
    # syringes = []
    usrmsglogger = DataLogger()
    evobot = EvoBot(PORT_NO, usrmsglogger)
    head = Head(evobot)
    sockets = evobot.getPopulatedSockets()
    decanol_syringe = Syringe(evobot, SYRINGE0)
    salt_syringe = Syringe(evobot, SYRINGE1)

    cv2.namedWindow('result')
    cap = cv2.VideoCapture(CAMERA_ID)
    cap.set(3, 1280)
    cap.set(4, 720)

    cv2.setMouseCallback('result', fai_cose, 0)

    while True:
        ret, frame = cap.read()
        cv2.imshow('result', frame)
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break

    cap.release()
    cv2.destroyAllWindows()

initialize()


