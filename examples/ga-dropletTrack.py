import sys
import random
from dropletB import DropletB
from deap import base, creator, tools
import pickle

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

IND_SIZE = 2

toolbox = base.Toolbox()
toolbox.register("attribute", random.random)
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attribute, n=IND_SIZE)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)


# the goal ('fitness') function to be minimized
def eval_minimization(individual):
    print "press enter to continue"
    text = sys.stdin.readline()

    print "INI DropletB"
    droplet_tr = DropletB()
    print "INI prepare decanoate"
    droplet_tr.prepare_decanoate(individual)
    print "INI perform exp"
    return droplet_tr.perform_experiment(),

    # print "how is the fitness value of the individual?"
    # fitness = float(sys.stdin.readline())
    # return fitness,

toolbox.register("evaluate", eval_minimization)
toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
toolbox.register("select", tools.selTournament, tournsize=2)


def main(population_file=None):
    # cross_prob  is the probability with which two individuals are crossed
    # mutation_prob is the probability for mutating an individual
    # num_generation  is the number of generations for which the evolution runs
    cross_prob, mutation_prob, num_generation = 0.5, 0.2, 4
    random.seed(64)

    if not population_file:
        # create an initial population of 300 individuals (where
        # each individual is a list of floats)
        pop = toolbox.population(n=3)  # enough per noi anche - o mettere a 5
        start_gen = 0
        # Evaluate the entire population
        fitnesses = list(map(toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit,
    else:
        cp = pickle.load(open(population_file, "r"))
        pop = cp["population"]
        start_gen = cp["generation"]
        random.setstate(cp["rndstate"])
        fitnesses = cp["fitness"]
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit,

    print("Start of evolution")

    # Begin the evolution
    for g in range(start_gen, num_generation):
        print("-- Generation %i --" % g)

        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop))

        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))

        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            # cross two individuals with probability cross_prob
            if random.random() < cross_prob:
                toolbox.mate(child1, child2)
                # fitness values of the children must be recalculated later
                del child1.fitness.values
                del child2.fitness.values

        for mutant in offspring:
            # mutate an individual with probability mutation_prob
            if random.random() < mutation_prob:
                toolbox.mutate(mutant)
                del mutant.fitness.values

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        print("  Evaluated %i individuals" % len(invalid_ind))

        # The population is entirely replaced by the offspring
        pop[:] = offspring

        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in pop]

        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x * x for x in fits)
        std = abs(sum2 / length - mean ** 2) ** 0.5

        print("  Min %s" % min(fits))
        print("  Max %s" % max(fits))
        print("  Avg %s" % mean)
        print("  Std %s" % std)

        cp = dict(population=pop, generation=g, fitness=fits, rndstate=random.getstate())
        filename = "population-g" + str(g) + ".pkl"
        pickle.dump(cp, open(filename, "wb"))

    print("-- End of (successful) evolution --")

    best_ind = tools.selBest(pop, 1)[0]
    print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()
