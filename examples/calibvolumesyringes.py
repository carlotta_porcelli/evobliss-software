import sys

sys.path.append('../api')
sys.path.append('../settings')

from configuration import *
from evobot import EvoBot
from datalogger import DataLogger
from syringe import Syringe
from head import Head
from petridish import PetriDish
from calibration import Calibration

'''This program can be used to calibrate the volume of a syringe
    Setting the vol variable  and the position from which the syringe has to pull it, it then moves the head home
    and it pushes that volume of liquid. Make sure to put some eppendorf under the syringe before it pushes liquid
'''

usrMsgLogger = DataLogger()
evobot = EvoBot(PORT_NO, usrMsgLogger)
head = Head(evobot)
syringe = Syringe(evobot, SYRINGE2)
syringe.plungerSetConversion(1)  # ml per mm of displacement of plunger
petridish = PetriDish(evobot, center=(150, 193), goalPos=-24, diameter=10, liquidType='water')
syringe.plungerMovePos(35)

vol = 10
head.move(10, 10)
syringe.syringeMove(-54)

syringe.plungerPullVol(vol)
syringe.syringeMove(0)
head.home()
syringe.syringeMove(-24)
syringe.plungerPushVol(vol)
syringe.syringeMove(0)

# syringe.home()
evobot.disconnect()
exit()