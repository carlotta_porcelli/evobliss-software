import random
import sys

sys.path.append('../api')
sys.path.append('../settings')

from evobot import EvoBot
from datalogger import DataLogger
from syringe import Syringe
from head import Head
from pump import Pump
from configuration import *


class DrinkMixer:
    def __init__(self):
        self.usrMsgLogger = DataLogger()
        self.evobot = EvoBot(PORT_NO, self.usrMsgLogger)
        self.head = Head(self.evobot)
        self.syringe = Syringe(self.evobot, SYRINGE0) #['SYRINGE1'])
        self.pumpSyringe = Syringe(self.evobot, SYRINGE1)#['SYRINGE2'])
        #self.pump = Pump(self.evobot, PUMPS['PUMP1'])

        self.head.setSpeed(4000)
        self.syringe.syringeSetSpeed(900)
        self.syringe.syringeSetAcc(900)
        self.syringe.plungerSetSpeed(288)
        self.syringe.plungerSetAcc(96)

        if not self.evobot.hasHomed():
            self.evobot.home()
        else:
            self.syringe.syringeMove(0)
            #self.pumpSyringe.syringeMove(0)

        self.syringe.plungerMoveToDefaultPos()

    def serve(self, liquids): # range between 0 e qualcosa'altro -> e' ph qualcosa
        sum = 0
        for i in range(0, 5):
            sum = sum + liquids[i] # run through the individual || frazioni di un cup full
        for id in range(0, 5): # 5 juices
            amount = round(float(44.0 * liquids[id] / sum), 2) # figure out the amount of ml RILEVANTE: solo le ultime 3 righe
            if amount > 1:
                self.getLiquidID(id, amount)
        self.addWater()

    def addWater(self):
        self.head.move(150, 15)
        self.pumpSyringe.syringeMove(-30)
        #self.pump.pumpPushVol(5)
        self.pumpSyringe.syringeMove(0)

    def getLiquid(self, x, y, amount):
        # get liquid
        self.head.move(x, y)
        self.syringe.syringeMove(-30)
        self.syringe.plungerPullVol(amount)
        self.syringe.syringeMove(0)

        # dispense liquid
        self.head.move(95, 15)
        self.syringe.syringeMove(-30)
        self.syringe.plungerPushVol(amount)
        self.syringe.syringeMove(0)

    def getLiquidID(self, id, amount):
        # get liquid
        positions = [[170, 215], [95, 215], [20, 215], [130, 135], [55, 135]]
        self.getLiquid(positions[id][0], positions[id][1], amount)

    def quit(self):
        self.evobot.disconnect()


if __name__ == "__main__":
    print "enter a number 1-5 (press any other number to quit):"
    drinkID = int(sys.stdin.readline())
    drinkmixer = DrinkMixer()
    if drinkID >= 0 and drinkID < 5:
        drinkmixer.getLiquidID(drinkID, 44)
    elif drinkID == 5:
        # mystery drink
        sum = 0.0
        liquids = []
        for i in range(0, 5):
            liquids.append(random.random())
        drinkmixer.serve(liquids)
    else:
        drinkmixer.quit()
        sys.exit(-1)

    drinkmixer.quit()
