""" Configuration file. Main configuration for the robot. DO NOT CHANGE.
"""


# For MAC. The numbers at the end could change.
PORT_NO = "/dev/tty.usbmodem14141"
# For Windows COMX, where X is the number of the port.
# PORT_NO = "COM5"

# head limits
HEAD = {'X_LIMIT': 185, 'Y_LIMIT': 225}

# CAMERA_ID = 0
IMAGE_SIZE = (1024, 768)
# ID is the slot the syringe is on,GOAL_POS is how much the syringe should go down to reach its goal. e.g. droplet
# i.e syringe1 is on slot 9, and should go down 30 mm
# SYRINGES = {
#     'SYRINGE1': {'ID': 0, 'SYRINGE_LIMIT': -68, 'PLUNGER_LIMIT': 35, 'GOAL_POS': -55, 'PLUNGER_CONVERSION_FACTOR': 1},
#     'SYRINGE2': {'ID': 1, 'SYRINGE_LIMIT': -68, 'PLUNGER_LIMIT': 45, 'GOAL_POS': -55, 'PLUNGER_CONVERSION_FACTOR': 1}
# }

try:
    from local import *
except ImportError:
    pass
