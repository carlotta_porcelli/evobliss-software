import cv2
import numpy as np
import sys
sys.path.append('../api')
sys.path.append('../settings')
from configuration import *
#import cv2.cv as cv

img = None
mask = None
global hsv

ix=50
iy=50
droplx=0
droply=0
hsv_pixel=[[[160,255,255]]]
#hUpper=179

M1= np.load('affinemat/'+ str(SYRINGES['SYRINGE1']['ID']) +'.npy')
A=M1[0:2,0:2]
detA= abs(np.linalg.det(A))

# mouse callback function

def center_coordinate(event,x,y,flags,param):
    global ix,iy, hsv_pixel,droplx,droply
    
    if event == cv2.EVENT_RBUTTONUP:
        
        ix=x
        iy = y
        
    elif event == cv2.EVENT_LBUTTONDOWN:
        droplx=x
        droply=y
        GBRpixel= img [x,y]
        #print GBRpixel
        hsvpix = np.uint8([[GBRpixel]])
        hsv_pixel = cv2.cvtColor(hsvpix,cv2.COLOR_BGR2HSV)
        #print "droplx,droply:" + str(droplx) + "," + str(droply)   
        

# Create a black image, a window and bind the function to window
img = np.zeros((512,512,3), np.uint8)
cv2.namedWindow('TrackBars', 0)
cv2.namedWindow('image')
#cv2.namedWindow('informations',0)
#cv2.putText(informations, "Hello World", cv2.FONT_HERSHEY_SIMPLEX, .6, 255)

#cv2.resizeWindow('result', 300, 300)
#cv2.resizeWindow('image', 100, 200)

#cv2.setMouseCallback('image',draw_circle)
cap = cv2.VideoCapture(0)
_,img = cap.read()
#khub=img
x, y, z = img.shape
mask2 = np.zeros((x, y, 3), np.uint8)
mask2.fill(255)
cv2.setMouseCallback('image',center_coordinate)


def nothing(x):
    pass


# Starting with 100's to prevent error while masking
h,s,v = 100,100,100

# Creating track bar in separated window
selectColor = '0 : Blue \n 1 : Red \n 2 : Green'

cv2.createTrackbar('hUpper', 'TrackBars',179,179,nothing)
cv2.createTrackbar('hLower', 'TrackBars',0,179,nothing)

cv2.createTrackbar('sUpper', 'TrackBars',255,255,nothing)
cv2.createTrackbar('sLower', 'TrackBars',0,255,nothing)

cv2.createTrackbar('vUpper', 'TrackBars',255,255,nothing)
cv2.createTrackbar('vLower', 'TrackBars',0,255,nothing)

cv2.createTrackbar('R', 'TrackBars',300,1000,nothing)
cv2.createTrackbar('selectColor', 'TrackBars',0,4,nothing)

while(1):

    _, frame = cap.read()
    
    mask2.fill(0)
    
    R = cv2.getTrackbarPos('R','TrackBars')
    cv2.circle(mask2, (ix, iy), R, (255, 255, 255), -1)
    frame = cv2.bitwise_and(frame, mask2)
    #cv2.imshow('frame',frame)

    #converting to HSV
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    # get info from track bar and appy to result
    selection = cv2.getTrackbarPos('selectColor','TrackBars')
    hLower = cv2.getTrackbarPos('hLower','TrackBars')    
    hUpper = cv2.getTrackbarPos('hUpper','TrackBars')
    sLower = cv2.getTrackbarPos('sLower','TrackBars')
    sUpper = cv2.getTrackbarPos('sUpper','TrackBars')
    vLower = cv2.getTrackbarPos('vLower','TrackBars')
    vUpper = cv2.getTrackbarPos('vUpper','TrackBars')
    

    if selection==0:
        
        lower_blue = np.array([hLower,sLower,vLower])
        upper_blue = np.array([hUpper,sUpper,vUpper])
        numberToColor="Manual Mode"

    elif selection==1:
        
        lower_blue = np.array([140,40,40])
        upper_blue = np.array([180,255,255])
        numberToColor="red"
    
    elif selection==2:
        lower_blue = np.array([100,50,50])
        upper_blue = np.array([160,255,255])
        numberToColor="blue"

    elif selection ==3:
        # Normal masking algorithm
        lower_blue = np.array([70,60,60])
        upper_blue = np.array([80,255,255])
        numberToColor="green"
        
    else:
        
        # Normal masking algorithm
        lower_blue = np.array([hLower,sLower,vLower])
        upper_blue = np.array([hUpper,sUpper,vUpper])
        numberToColor="other"    

    
    mask = cv2.inRange(hsv,lower_blue, upper_blue)

    result = cv2.bitwise_and(frame,frame,mask = mask)
    
     
    _,contours,hierarchy = cv2.findContours(mask,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
        
        
        #CalcContourProperties
        
  
        # finding contour with maximum area and store it as best_cnt
    max_area = 0
        #best_cnt=0
        
        #print contours
        #print type(contours)
    
    for cnt in contours:
        
        area = cv2.contourArea(cnt)
        approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
        
        if area > 50 and area<80000:
            max_area = area
            best_cnt = cnt
            # finding centroids of best_cnt and draw a circle there
            M = cv2.moments(best_cnt)
            # cx.cy sono le coordinate dei centri
            cx,cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
            rows,cols,ch = best_cnt.shape
            
            #print cx,cy
           
            cv2.drawContours(result, best_cnt, -1, (0,255,0), 1)
            cv2.putText(result,"Area(pixel):" + str(max_area), (cx+20,cy+60), cv2.FONT_HERSHEY_SIMPLEX, .6, 255)
            cv2.putText(result,"Area(mm2):" + str(max_area* detA), (cx+20,cy+80), cv2.FONT_HERSHEY_SIMPLEX, .6, 255)
            cv2.putText(result,"Droplet Center(pixel):" + str(cx) + ',' +str(cy), (cx+20,cy+100), cv2.FONT_HERSHEY_SIMPLEX, .6, 255)
            
            centerPixel=np.float32([cx,cy,1])
            CenterCor=np.dot(M1, centerPixel)
                
            cv2.putText(result,"Droplet Center(mm):" + str(CenterCor[0]) + ',' +str(CenterCor[1]), (cx+20,cy+120), cv2.FONT_HERSHEY_SIMPLEX, .6, 255)
            print('Number of contours: {0}'.format(len(contours)))
            #cv2.imshow('frame',frame)
            cv2.circle(result,(cx,cy),5,255,-1)
                

            #cv2.circle(frame,(cx,cy),5,255,-1)
    
    #result = cv2.bitwise_and(result, mask2)
    
    hrange= "Hue range:" + str(hLower) + "_" + str(hUpper)
    cv2.putText(result,hrange, (50,320), cv2.FONT_HERSHEY_SIMPLEX, .7, 255)
    srange= "Saturation range:" + str(sLower) + "_" + str(sUpper)
    cv2.putText(result,srange, (50,340), cv2.FONT_HERSHEY_SIMPLEX, .7, 255)
    vrange= "Value range:" + str(vLower) + "_" + str(vUpper)
    cv2.putText(result,vrange, (50,360), cv2.FONT_HERSHEY_SIMPLEX, .7, 255)
    Radius= "Radius:" + str(R)
    cv2.putText(result,Radius, (50,380), cv2.FONT_HERSHEY_SIMPLEX, .7, 255)
    ColorToDetect= "Color to Detect:" + str(numberToColor)
    
    cv2.putText(result,ColorToDetect, (50,400), cv2.FONT_HERSHEY_SIMPLEX, .7, 255)
    cv2.putText(result,str(hsv_pixel), (droplx,droply), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
    cv2.putText(result,"coordinate(pixel):" + str(droplx)+","+ str(droply), (droplx,droply+20), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
    impixel=np.float32([droplx,droply,1])
    RobCor=np.dot(M1, impixel)
    
    cv2.putText(result,"coordinate(mm):" + str(RobCor[0])+","+ str(RobCor[1]), (droplx,droply+40), cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
    

    cv2.imshow('image',result)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cap.release()

cv2.destroyAllWindows()