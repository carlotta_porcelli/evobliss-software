import sys

sys.path.append('../api')
sys.path.append('../settings')

from configuration import *
from evobot import EvoBot
from datalogger import DataLogger
from syringe import Syringe
from head import Head
from petridish import PetriDish
from calibration import Calibration

usrMsgLogger = DataLogger()
evobot = EvoBot(PORT_NO, usrMsgLogger)
head = Head(evobot)
syringe = Syringe(evobot, SYRINGE0)
syringe.plungerSetConversion(1)  # ml per mm of displacement of plunger
petridish = PetriDish(evobot, center=(160, 120), goalPos=-20, diameter=10, liquidType='water')

calibration = Calibration(evobot, head, petridish, syringe)
calibration.affineCalibrate()
syringe.home()
exit()
