import threading

import time


class Pump:
    """
    This class encapsulated methods for controlling the pump of the robot
    """

    def __init__(self, _evobot, pumpConfig):
        """
        This method initializes the pump class. The key parameter is evobot
        to which this head is attached and the x and y limits of the robot
        given in mm.
        """
        self.evobot = _evobot

        # these are only updated upon receiving new positions form robot
        self.e = -1
        self.eOrg = -1  # Original e, before rounding
        self.pumpConversion = pumpConfig['PUMP_CONVERSION_FACTOR']  # ml per mm

        self.dataLogger = None
        self.evobot.heads.append(self)
        self.event = threading.Event()
        self.logBuffer = [0, 0, 0, 0, 0]
        self.logTitlesNeeded = True
        self.timeNow = None
        self.pumpSpeed = 80

    def _recvcb(self, line):
        """
        This is an internal use method that is used to parse messages from the robot
        """

        terms = []
        if line.startswith('X'):
            terms = str(line).split()
            if terms[9].startswith('E'):
                self.eOrg = float(terms[9][2:])
                self.e = round(self.eOrg, 2)
            self.newPositionAvailable = True
            if self.dataLogger is not None:
                self.timeNow = time.time() - self.evobot.iniTime
                eSpeed = (self.eOrg - self.logBuffer[1]) / (self.timeNow - self.logBuffer[0])

                try:
                    if self.dataLogger.kind == 'dat':
                        self.dataLogger(str(self.timeNow) + ' ' + str(self.eOrg) + "\n")
                    elif self.dataLogger.kind == 'csv':
                        if self.logTitlesNeeded:
                            self.dataLogger(('time', 'eSpeed'))
                            self.logTitlesNeeded = False

                        self.dataLogger((str(self.timeNow), str(self.eOrg)))
                except:
                    pass

                self.logBuffer = [self.timeNow, self.eOrg]

            self.event.set()

    def _updatePositionFromRobot(self):
        self.evobot.send('M114')
        self.event.wait()
        self.event.clear()

    def setDataLogger(self, logger):
        """
        This sets the data logger for the head. The data consist
        of the time, x, and y postions of the robot. The argument
        is a function that takes a string as input.
        """
        self.dataLogger = logger

    def pumpMovePos(self, e):
        """
        This method moves the head to the position (x,y) specified in mm.
        The method blocks the caller until the robot has completed
        the movement.
        """

        e = round(e, 2)
        # self.evobot._logUsrMsg( 'pump moving to: ' + str( e ) )

        moveToMsg = 'G1 E%f F%f' % (e, self.pumpSpeed)

        self.evobot.send(moveToMsg)
        self._updatePositionFromRobot()
        while not e == self.e:
            self._updatePositionFromRobot()
            # self.evobot._logUsrMsg( 'pump moved')

    def getE(self):
        """
        This returns the current x position of the robot
        """
        return self.e

    def pumpSetConversion(self, factor):  # mm per ml
        """
        This is method used for calibrating the pump.
        It sets the mm per ml of the plunger
        """

        self.pumpConversion = factor

    def pumpGetConversion(self):
        """
        This method returns the current mm per ml convertion factor of the
        pump motor.
        """
        return self.pumpConversion

    def setDataLogger(self, logger):
        """
        This sets the data logger for the head. The data logged consist
        of the time, plunger position, and syringe position. The argument
        is a function that takes a string as input.
        """

        self.dataLogger = logger

    def pumpPullVol(self, ml):
        """
        This method pulls ml of liquid.
        """
        self._updatePositionFromRobot()
        self.evobot._logUsrMsg('Pump pulling ' + str(ml))
        self.pumpMovePos(self.e - ml * self.pumpConversion)
        self.evobot._logUsrMsg('Pump pulled')

    def pumpPushVol(self, ml):
        """
        This method pushes ml of liquid.
        """
        self._updatePositionFromRobot()
        self.evobot._logUsrMsg('Pump pushing ' + str(ml))
        self.pumpMovePos(self.e + ml * self.pumpConversion)
        self.evobot._logUsrMsg('Pump pushed')

    def pumpMoveVol(self, ml):
        """
        This method is convenience function that pulls ml of liquid
        if ml is negative and pushes ml liquid if ml is positive.
        """
        if (ml < 0):
            self.pumpPullVol(-ml)
        else:
            self.pumpPushVol(ml)

    def setSpeed(self, goalSpeed):
        """
        This method sets the speed of the pump of the robot
        """
        if goalSpeed < 1 or goalSpeed > 100:
            self.evobot._logUsrMsg('Pump attempted to set its maximum speed to ' + str(
                goalSpeed) + 'mm/min, but exceed pump speed\'s limits of [1:100]')
            self.evobot.quit()

        goalSpeed = round(goalSpeed, 0)

        self.evobot._logUsrMsg('Setting pump maximum speed to ' + str(goalSpeed) + 'mm/min')
        self.pumpSpeed = goalSpeed

        self.evobot._logUsrMsg('Pump speed set')
