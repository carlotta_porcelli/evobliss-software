import cv2
import math
import numpy as np
import datetime
from collections import defaultdict
import sys
from myDroplet import MyDroplet
sys.path.append('../api')
sys.path.append('../settings')
# from configuration import CAMERA_ID

ix = 40
iy = 60
R = 1

other_start_position = None
salt_position = None

window_size = (1280, 720)

paths = defaultdict(list)
distances = defaultdict(list)

dropCount = 0
droplets = []

# RED COLOR DEFINITION
lower = np.array([0, 40, 40])
upper = np.array([8, 255, 255])
lower2 = np.array([140, 40, 40])
upper2 = np.array([180, 255, 255])

# SETTINGS
SENSITIVITY_VALUE = 23  # Blurring function parameter
BLUR_SIZE = 14  # Blurring function parameter

MIN_AREA = 10  # Minimum area for droplets to be recognized
MOVEMENT_TOLERANCE = 5  # Limit for updating droplet positi2on
AREA_TOLERANCE = 40  # Limit of area change for updating

debug_mode = False
recording = False

date = ""
fourCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')


class Tracking:
    def __init__(self, frame, affinemat):
        self.frame = frame
        self.affineMat = affinemat
        self.x = None
        self.y = None

    def float_euclidean_dist(self, p, q):
        px = p[0]
        py = p[1]
        qx = q[0]
        qy = q[1]
        diff_x = abs(qx - px)
        diff_y = abs(qy - py)
        return float(math.sqrt((diff_x * diff_x) + (diff_y * diff_y)))

    def get_similar_index(self, drop):
        for c in range(0, len(droplets)):
            distance = self.float_euclidean_dist(droplets[c].centroid, drop.centroid)
            if distance < 50:
                return c
            else:
                continue
        return -1

    def is_in_the_array(self, drop):  # IMPROVABLE
        for drp in droplets:
            distance = self.float_euclidean_dist(drp.centroid, drop.centroid)
            if distance < 50:
                return True
        return False

    def track(self, thresh_image, result, color):

        global droplets, dropCount
        _, contours, hierarchy = cv2.findContours(thresh_image, cv2.RETR_EXTERNAL,
                                                  cv2.CHAIN_APPROX_NONE)  # Finds the contours in the image
        refarea = 0

        if len(contours) > 0:
            dropCount = len(hierarchy)

        for cnt in contours:
            mom = cv2.moments(cnt)
            area = mom['m00']
            if area > 5 and area > refarea:
                centroid = (int(mom['m10'] / area), int(mom['m01'] / area))
                refarea = area
                drop = MyDroplet(dropCount, centroid, cnt, color, area)
                paths[drop.dropId].append(drop.centroid)   # mette una droplet nell'array
                cv2.drawContours(result, cnt, -1, (0, 255, 0), 1)
                message = "Color : " + str(color)
                cv2.putText(result, message, centroid, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
                if not self.is_in_the_array(drop):
                    print "Added droplet " + str(dropCount)
                    droplets.append(drop)
                else:
                    index = self.get_similar_index(drop)
                    if index == -1:
                        print "ERROR"
                        exit()

    def do_track(self, frame, result):
        """ One shot: identify the droplet and display it in the image """

        # Image blurring and thresholding
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # RED TRACKING
        thresh_image = cv2.bitwise_or(cv2.inRange(frame_hsv, lower, upper), cv2.inRange(frame_hsv, lower2, upper2))
        cv2.blur(thresh_image, (5, 5), thresh_image)
        cv2.threshold(thresh_image, 0, 255, cv2.THRESH_OTSU, thresh_image)
        cv2.morphologyEx(thresh_image, cv2.MORPH_OPEN, (20, 20), thresh_image)
        cv2.morphologyEx(thresh_image, cv2.MORPH_CLOSE, (20, 20), thresh_image)
        self.track(thresh_image, result, "red")
        # cv2.imshow('Threshold Image', thresh_image)

    def show_output(self, result):
        print "COMPLETE PATH :"
        for drp in droplets:
            for point in drp.path:
                print point

        print "DISTANCES :"
        for drp in droplets:
            for c in distances[drp.dropId]:
                print c

        for drp in droplets:
            starting_point = drp.path[0]
            ending_point = drp.path[0]

        for index, c in enumerate(paths[drp.dropId]):
            cv2.circle(result, c, 1, (255, 0, 0), 1)
            if index == 0:
                print('inizio', c)
                cv2.putText(result, "BEGIN", c, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            elif index == len(paths[drp.dropId]) - 1:
                print('fine', c)
                cv2.putText(result, "END", c, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
        return starting_point, ending_point
