import sys
import time
import random
import Queue
from threading import Thread
sys.path.append('../settings')
from configuration import *

maxSyringes = 32

class robot:
    isOpen = False
    
    def __init__(self):
        self.headXcurrent = random.randint(0, 100)
        self.headYcurrent = random.randint(0, 100)
        self.headXtarget =  self.headXcurrent
        self.headYtarget =  self.headYcurrent

        self.syringeTargets = []
        self.syringeCurrents = []
        self.plungerTargets = []
        self.plungerCurrents = []
        
        i = 0
        while i<maxSyringes:          
            self.syringeCurrents.append( -random.randint(0, 30) )
            self.syringeTargets.append( self.syringeCurrents[i] )
            self.plungerCurrents.append( -random.randint(0, 40) )
            self.plungerTargets.append( self.plungerCurrents[i] )
            i=i+1

        self.motionControlRun = True
        self.motionControlThread = None
        self.messageBuffer = Queue.Queue()
        self.hasHomed = False

    def step( self, current, target ):
        if abs( current - target ) < 1:
            current = target
        if ( current > target):
            current-=1
        elif ( current < target):
            current+=1
        return current

    def motionControl(self):
        while( self.motionControlRun ):
            self.headXcurrent = self.step( self.headXcurrent, self.headXtarget )
            self.headYcurrent = self.step( self.headYcurrent, self.headYtarget )
            i = 0
            while i<maxSyringes:
                self.syringeCurrents[i] = self.step( self.syringeCurrents[i], self.syringeTargets[i] )
                self.plungerCurrents[i] = self.step( self.plungerCurrents[i], self.plungerTargets[i] )
                i=i+1
            time.sleep(0) #0.05
                
    def close(self):
        self.isOpen = False
        self.motionControlRun = False
        if self.motionControlThread is not None:
            self.motionControlThread.join()
            
    def connect(self, port, baud):
        self.motionControlThread = Thread(target=self.motionControl).start()
        self.isOpen = True

    def reset(self):
        print "RESET"

    def readline(self):
        for i in range (0,10):
            if not self.messageBuffer.empty() and self.motionControlRun:
                break
            time.sleep(0.05)

        if (not self.motionControlRun):
            self.messageBuffer.put("EOF")

        if self.messageBuffer.empty():
            self.messageBuffer.put("")
            
        return(self.messageBuffer.get())

    def write( self, string ):
        # handles
        # N0 M291*41 - home syringe and plunger
        # N1 G28*18 - home head
        # N2 M114*37 - get current head position

        terms = string.split()

        if (terms[1].startswith('M292')):
            self.messageBuffer.put('ok')
            mesg = "PS"
            for j in SYRINGES:
                mesg += " " + str(SYRINGES[j]['ID'])
            self.messageBuffer.put(mesg )
        if (terms[1].startswith('M291')):
            self.messageBuffer.put('ok')
            syringeID = int( terms[2].split('*')[0][1:] )
            self.syringeTargets[syringeID] = 0
            self.plungerTargets[syringeID] = 0            
        if (terms[1].startswith('G28')):
            self.messageBuffer.put('ok')
            self.headXtarget = 0
            self.headYtarget = 0
            self.hasHomed = True
        if (terms[1].startswith('M114')):
            self.messageBuffer.put('ok')
            self.messageBuffer.put("X:%f Y:0.0 Z:%f Current Pos X: %f Y:0.0 Z:%f" % (self.headXtarget,self.headYtarget,self.headXcurrent,self.headYcurrent ) )
        if (terms[1].startswith('G1')):            
            self.messageBuffer.put('ok')
            self.headXtarget = float( terms[2][1:] )
            self.headYtarget = float( terms[3].split('*')[0][1:] )
        if (terms[1].startswith('M290')):
            self.messageBuffer.put('ok')
            if (terms[2].startswith('I')):
                self.syringeID = int( terms[2].split('*')[0][1:] )
            if len(terms) < 4:
                 self.messageBuffer.put("I " + str(self.syringeID) + " S " + str( self.syringeCurrents[self.syringeID] ) + " P " + str( self.plungerCurrents[self.syringeID] ))
            else:
                if (terms[3].startswith('S')):
                    self.syringeTargets[self.syringeID] = float( terms[3].split('*')[0][1:] )
                if (terms[3].startswith('P')):
                    self.plungerTargets[self.syringeID] = float( terms[3].split('*')[0][1:] )
        if (terms[1].startswith('M298')):
            self.messageBuffer.put('ok')
            self.messageBuffer.put('HOMED FALSE')
        if (terms[1].startswith('M112')):
            self.motionControlRun = False
        if (terms[1].startswith('M285')):
            self.messageBuffer.put('ok')
            self.messageBuffer.put("V 0 M " + str( random.random()*5 ) )
        time.sleep(0)

                       
