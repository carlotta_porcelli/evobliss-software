import numpy as np
import os
import threading
import time


class Syringe:
    def __init__(self, _evobot, syringeConfig):
        """
        This method initialize the syringe object. The parameters
        are the evobot object to which this syringe is attached.
        The ID of this syringe object and the movement limits of
        the syringe and the plunger in mm
        """
        self.syringeID = syringeConfig['ID']
        self.syringeGoalPos = syringeConfig['GOAL_POS']
        self.evobot = _evobot

        if not str( self.syringeID ) in self.evobot.populatedSockets:
            self.evobot._logUsrMsg( 'Syringe initialization failed: no syringe on socket ' + str( self.syringeID ) )
            self.evobot._logUsrMsg( 'Available sockets: ' + str( self.evobot.populatedSockets ) )
            self.evobot.quit()
            return

        self.syringePos = -1 # mm
        self.syringeLimit = syringeConfig['SYRINGE_LIMIT'] # mm

        self.plungerPos = -1 # mm
        self.plungerLimit = syringeConfig['PLUNGER_LIMIT']   # this is when the plunger is completely push down - corresponding to plungerPos = 0
        self.plungerConversion = syringeConfig['PLUNGER_CONVERSION_FACTOR'] #ml per mm
        self.dateLogger = None
        self.evobot.modules.append( self )
        self.event = threading.Event()
        self.logBuffer=[0,0,0,0,0]
        self.logTitlesNeeded= True   
        fname = '../calibration/affinemat/'+str(self.syringeID) + '.npy'
        if os.path.isfile(fname):    
            self.affineMat=np.load(fname)
        else:
            self.affineMat = None
                
    def _recvcb(self, line):
        """
        Private methods that handles messages received from the robot
        """
        
        terms = []
        if line.startswith( 'I' ):
            terms = str(line).split( )
            if int(terms[1]) == self.syringeID:
                self.syringePos = round(float( terms[3] ),2) 
                self.plungerPos = round(float( terms[5] ),2)
                self.event.set()                
                if self.dataLogger is not None:
                    timeNow=time.time() - self.evobot.iniTime
                    sSpeed=(self.syringePos-self.logBuffer[1]) / (timeNow-self.logBuffer[0])
                    pSpeed=(self.plungerPos-self.logBuffer[2]) / (timeNow-self.logBuffer[0])
                    sAcceleration=(sSpeed-self.logBuffer[3]) / (timeNow-self.logBuffer[0])
                    pAcceleration=(pSpeed-self.logBuffer[4]) / (timeNow-self.logBuffer[0])                                 
                    try:
                        if self.dataLogger.kind=='dat':
                            self.dataLogger( str( time.time() - self.evobot.iniTime ) + ' ' + str(self.syringePos) + ' ' + str(self.plungerPos) + "\n")
                        elif self.dataLogger.kind=='csv':
                            if self.logTitlesNeeded:
                                self.dataLogger(('time','syringePos','plungerPos','sSpeed','pSpeed','sAcceleration', 'pAcceleration'))
                                self.logTitlesNeeded= False
                                                      
                            self.dataLogger( (str( timeNow ), str(self.syringePos), str(self.plungerPos), str(sSpeed), str(pSpeed), str(sAcceleration), str(pAcceleration) ) )
                    except:
                        pass  
                    self.logBuffer=[timeNow,self.syringePos,self.plungerPos,sSpeed,pSpeed]

    def _getPos(self):
        """
        Private method that is internally used in the class to obtain
        the positions of both the syringe and the plunger.
        """
        
        syringeGetPosMsg = 'M290 I' + str(self.syringeID)
        self.evobot.send( syringeGetPosMsg )        
        self.event.wait()
        self.event.clear()
            
    def plungerGetPos(self):
        """
        This method returns the position of the plunger in ml.
        """
        
        self._getPos()
        return self.plungerPos

    def syringeGetPos(self):
        """
        This method returns the position of the syringe in mm.
        """
        
        self._getPos()
        return self.syringePos
                   
    def plungerSetConversion(self, factor):   #mm per ml
        """
        This is method used for calibrating the plunger.
        It sets the mm per ml of the plunger
        """
        
        self.plungerConversion = factor
        
    def plungerGetConversion(self):
        """
        This method returns the current mm per ml convertion factor of the
        plunger motor.
        """        
        return self.plungerConversion
    
    def plungerSetSpeed(self, goalSpeed): #steps/s
        """
        Method that sets the maximum speed of the movement of the plunger.
        """
        if goalSpeed < 12 or goalSpeed > 288:
            self.evobot._logUsrMsg( 'Plunger attempted to set its maximum speed to '  +  str( goalSpeed ) + 'steps/s, but exceed plunger ' + str( self.syringeID ) + 'speed\'s limits of [12:288] (0.5:12 turns/s)'  )
            self.evobot.quit()

        goalSpeed = round(goalSpeed, 0)
                
        self.evobot._logUsrMsg( 'Setting plunger maximum speed to ' + str( goalSpeed ) + 'steps/s' )
        plungerSetSpeedMsg = 'M295 I' + str(self.syringeID) + ' P' + str( goalSpeed )
        self.evobot.send( plungerSetSpeedMsg )
        
        self.evobot._logUsrMsg( 'Plunger speed set' )
        
    def plungerSetAcc(self, goalAcc): #steps/s
        """
        Method that sets the acceleration of the movement of the plunger.
        """
        if goalAcc < 12 or goalAcc > 96:
            self.evobot._logUsrMsg( 'Plunger attempted to set its acceleration to '  +  str( goalAcc ) + 'steps/s2, but exceed plunger ' + str( self.syringeID ) + 'acceleration\'s limits of [12:96] (0.5:4 turns/s2)'  )
            self.evobot.quit()

        goalAcc = round(goalAcc, 0)
                
        self.evobot._logUsrMsg( 'Setting plunger maximum acceleration to ' + str( goalAcc ) + 'steps/s' )
        plungerSetAccMsg = 'M296 I' + str(self.syringeID) + ' P' + str( goalAcc )
        self.evobot.send( plungerSetAccMsg )
        
        self.evobot._logUsrMsg( 'Plunger acceleration set' )
        
    def syringeSetSpeed(self, goalSpeed): #steps/s
        """
        Method that sets the maximum speed of the movement of the syringe.
        """
        if goalSpeed < 50 or goalSpeed > 1500:
            self.evobot._logUsrMsg( 'Syringe attempted to set its maximum speed to '  +  str( goalSpeed ) + 'steps/s, but exceed syringe ' + str( self.syringeID ) + 'speed\'s limits of [50:1500] (0.25:7.5 turns/s)'  )
            self.evobot.quit()

        goalSpeed = round(goalSpeed, 0)
                
        self.evobot._logUsrMsg( 'Setting syringe maximum speed to ' + str( goalSpeed ) + 'steps/s' )
        syringeSetSpeedMsg = 'M295 I' + str(self.syringeID) + ' S' + str( goalSpeed )
        self.evobot.send( syringeSetSpeedMsg )
        
        self.evobot._logUsrMsg( 'Syringe speed set' )
        
    def syringeSetAcc(self, goalAcc): #steps/s
        """
        Method that sets the acceleration of the movement of the syringe.
        """
        if goalAcc < 50 or goalAcc > 1500:
            self.evobot._logUsrMsg( 'Syringe attempted to set its acceleration to '  +  str( goalAcc ) + 'steps/s2, but exceed syringe ' + str( self.syringeID ) + 'acceleration\'s limits of [50:1500] (0.25:7.5 turns/s2)'  )
            self.evobot.quit()

        goalAcc = round(goalAcc, 0)
                
        self.evobot._logUsrMsg( 'Setting syringe maximum acceleration to ' + str( goalAcc ) + 'steps/s' )
        syringeSetAccMsg = 'M296 I' + str(self.syringeID) + ' S' + str( goalAcc )
        self.evobot.send( syringeSetAccMsg )
        
        self.evobot._logUsrMsg( 'Syringe acceleration set' )

    def setDataLogger(self, logger):
        """
        This sets the data logger for the head. The data logged consist
        of the time, plunger position, and syringe position. The argument
        is a function that takes a string as input.
        """

        self.dataLogger = logger

    def syringeMove(self, goalPos): #mm
        """
        This move the syringe mm. This is a blocking call, hence, control
        is not returned to the caller until the syringe has finished moving.
        """
        
        if goalPos > 0 or goalPos < self.syringeLimit:
            self.evobot._logUsrMsg( 'Syringe attempt to move past limits of [0:' + str( self.syringeLimit) + ']' )
            return
            #self.evobot.quit()

        goalPos = round(goalPos, 2)
        
        self.evobot._logUsrMsg( 'Syringe moving to ' + str(goalPos) + 'mm (absolute)')
        
        syringeMoveMsg = 'M290 I' + str(self.syringeID) + ' S' + str(goalPos)
        self.evobot.send( syringeMoveMsg )
        while not self.syringeGetPos() == goalPos:
            pass
        self.evobot._logUsrMsg( 'Syringe moved' )

    def plungerMovePos( self, goalPos ): #mm
        """
        Method that controls the movement of the plunger.
        """
        if goalPos < 0 or goalPos > self.plungerLimit:
            self.evobot._logUsrMsg( 'Plunger attempted to move to '  +  str( goalPos ) + 'mm, but exceed plunger ' + str( self.syringeID ) + '\'s limits of [0:' + str( self.plungerLimit ) + ']'  )
            self.evobot.quit()

        goalPos = round(goalPos, 2)
                
        self.evobot._logUsrMsg( 'Plunger moving to ' + str( goalPos ) + 'mm (absolute)' )
        plungerMoveMsg = 'M290 I' + str(self.syringeID) + ' P' + str( goalPos )
        self.evobot.send( plungerMoveMsg )
        
        while not self.plungerGetPos() ==  round(float(goalPos),2):
            pass
        self.evobot._logUsrMsg( 'Plunger moved' )
                    
    def plungerPullVol(self, ml ):
        """
        This method pulls ml of liquid.
        """
        
        self.evobot._logUsrMsg( 'Plunger pulling ' + str(ml ) )
        self.plungerMovePos( self.plungerPos - ml*self.plungerConversion )
        self.evobot._logUsrMsg( 'Plunger pulled' )

    def plungerPushVol(self, ml ):
        """
        This method pushes ml of liquid.
        """

        self.evobot._logUsrMsg( 'Plunger pushing ' + str(ml ) )
        self.plungerMovePos( self.plungerPos + ml*self.plungerConversion )
        self.evobot._logUsrMsg( 'Plunger pushed' )

    def plungerMoveVol( self, ml ):
        """
        This method is convenience function that pulls ml of liquid
        if ml is negative and pushes ml liquid if ml is positive.
        """
        if ( ml < 0 ):
            self.plungerPullVol( -ml )
        else:
            self.plungerPushVol( ml )

    def plungerMoveToDefaultPos(self):
        """
        This a helper method that moves the plunger to its limit.
        In order words, prepare the syringe for pulling liquid.
        """
                
        self.evobot._logUsrMsg( 'Plunger moving to ' + str(self.plungerLimit) )
        self.plungerMovePos( self.plungerLimit )
        self.evobot._logUsrMsg( 'Plunger moved' )
            
    def home(self):
        """
        This homes the syringe and the plunger by moving them to their
        upper most position where the homing switches are activated.
        """
        
        self.evobot._logUsrMsg( 'homing syringe ' + str(self.syringeID) )    
        self.evobot.send( 'M291 I' + str( self.syringeID ) )
        while not self.syringeGetPos() == 0:
            pass
        if not self.plungerPos == 0:
            pass
        self.evobot._logUsrMsg( 'homed syringe' )

    def park(self):
        self.syringeMove( self.syringeLimit )

    def fillVolFrom(self, head, ml, container):
        center=container.worldCor.worldCorFor(container.center,self.syringeID)
        if ml is 'all':
            ml=self.plungerPos
        self.syringeMove(container.goalPos + container.edgeHeight)
        head.move(center[0],center[1])
        self.syringeMove(container.goalPos)
        self.plungerPullVol(ml)
        self.syringeMove(container.goalPos + container.edgeHeight)

    def emptyVolTo(self, head, ml, container):
        center=container.worldCor.worldCorFor(container.center,self.syringeID)
        if ml is 'all':
            ml=self.plungerLimit- self.plungerPos
        self.syringeMove(container.goalPos + container.edgeHeight)
        head.move(center[0],center[1])
        self.syringeMove(container.goalPos)
        self.plungerPushVol(ml)
        self.syringeMove(container.goalPos + container.edgeHeight)
        
    def syringeWash(self, head, times=3, offset=10, container=None):
        center = container.worldCor.worldCorFor(container.center, self.syringeID)
        liquidVol = self.plungerLimit - self.plungerPos
        self.syringeMove(0)
        head.move(center[0], center[1])
        self.syringeMove(container.goalPos+ offset)
        self.plungerPushVol(liquidVol)
        for time in xrange(0, times):
            self.syringeMove(container.goalPos)
            self.plungerPullVol(self.plungerLimit)
            self.syringeMove(container.goalPos+ offset)
            self.plungerPushVol(self.plungerLimit)            
            
        self.syringeMove(0)
        
    def goToXY(self, head, point, worldcor):
        
        mm=worldcor.worldCorFor((point[0],point[1]), self.syringeID)
        head.move( round(float(mm[0]),2), round(float(mm[1]),2) ) 

    def isEmpty(self):
        if round(float(self.plungerPos),2) >= round(float(self.plungerLimit),2):
            return True
        else:
            return False

    def isFull(self):
        if round(float(self.plungerPos),2)==0:
            return True
        else:
            return False
        
    def canDispenseVol(self,vol):
        if round(float(self.plungerLimit),2) - round(float(self.plungerPos),2) >= round(float(vol),2):
            return True
        else:
            return False

    def canAbsorbVol(self, vol):
        if round(float(self.plungerPos),2) >= round(float(vol),2):
            return True
        else:
            return False

    def getXY(self, head, worldcor):
        mm = worldcor.inverseWorldCorFor((head.getX(), head.getY()), self.syringeID)
        return round(float(mm[0]), 2), round(float(mm[1]), 2)
