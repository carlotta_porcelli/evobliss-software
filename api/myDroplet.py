import math
from collections import defaultdict

MOVEMENT_TOLERANCE = 5

# paths = defaultdict(list)


def floatEuclideanDist(p, q):
    px = p[0]
    py = p[1]
    qx = q[0]
    qy = q[1]
    diffX = abs(qx - px)
    diffY = abs(qy - py)
    return float(math.sqrt((diffX * diffX) + (diffY * diffY)))


class MyDroplet:
    def __init__(self, dropId, centroid, contour, color, area):
        self.dropId = dropId
        self.centroid = centroid
        self.contour = contour
        self.color = color
        self.area = area
        self.path = [centroid]
        # paths[self.dropId].append(centroid)

    def updateDroplet(self, centroid, contour, area):
        if floatEuclideanDist(self.centroid, centroid) > MOVEMENT_TOLERANCE:
            self.centroid = centroid
            self.contour = contour
            self.area = area
            print str(self.dropId) + " has been updated "
            # paths[self.dropId].append(centroid)
