import cv2
import math
import numpy as np
import datetime
from collections import defaultdict
import sys

sys.path.append('../api')
sys.path.append('../settings')
from newDroplet import NewDroplet
# from configuration import CAMERA_ID

ix = 40
iy = 60
R = 1

other_start_position = None
salt_position = None

window_size = (1280, 720)


# RED COLOR DEFINITION
lower = np.array([0, 40, 40])
upper = np.array([8, 255, 255])
lower2 = np.array([140, 40, 40])
upper2 = np.array([180, 255, 255])

# SETTINGS
SENSITIVITY_VALUE = 23  # Blurring function parameter
BLUR_SIZE = 14  # Blurring function parameter

MIN_AREA = 5  # Minimum area for droplets to be recognized
MOVEMENT_TOLERANCE = 5  # Limit for updating droplet position
AREA_TOLERANCE = 40  # Limit of area change for updating
DIST_TOLERANCE = 20  # Limit to decide if the droplet is a new droplet or it is an existing one

debug_mode = False
recording = False

date = ""
fourCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')


class MultiDropletTracking:
    def __init__(self):
        self.x = None
        self.y = None
        self.paths = defaultdict(list)
        self.distances = defaultdict(list)
        self.droplets = []
        self.previousDroplets = []
        
    def find_repeat(self, numbers):
        seen = set()
        for num in numbers:
            if num in seen:
                return num
            seen.add(num)
        
    
    def float_euclidean_dist(self, p, q):
        px = p[0]
        py = p[1]
        qx = q[0]
        qy = q[1]
        diff_x = abs(qx - px)
        diff_y = abs(qy - py)
        return float(math.sqrt((diff_x * diff_x) + (diff_y * diff_y)))

    def get_similar_index(self, drop):
        self.distances=[]
        for c in range(0, len(self.previousDroplets)):
            distance = self.float_euclidean_dist(self.previousDroplets[c].centroid, drop.centroid)
            self.distances.append(distance)
            
        if min(self.distances) < DIST_TOLERANCE:
            minDist=100000000
            i=0
            for d in self.distances:    
                if d < minDist:
                    minDist=d
                    index=i
                i+=1
            iD = self.previousDroplets[index].dropId     
            return iD
        else:
            return -1

    def was_in_the_array(self, drop):  # IMPROVABLE
        for drp in self.previousDroplets:
            distance = self.float_euclidean_dist(drp.centroid, drop.centroid)
            if distance < DIST_TOLERANCE:
                return True
        return False

    
    def track(self, thresh_image, result, color):

        _, contours, hierarchy = cv2.findContours(thresh_image, cv2.RETR_EXTERNAL,
                                                  cv2.CHAIN_APPROX_NONE)  # Finds the contours in the image


        dropCount=0###FAI    

        currentDroplets = []
        for cnt in contours:
            mom = cv2.moments(cnt)
            area = mom['m00']
            if area > MIN_AREA : 
                centroid = (int(mom['m10'] / area), int(mom['m01'] / area))
                drop = NewDroplet(dropCount, centroid, cnt, color, area)
                dropCount+=1
                currentDroplets.append(drop)
                if self.was_in_the_array(drop):
                    index = self.get_similar_index(drop)
                    drop.dropId = index
                    if index == -1:
                        print "ERROR"
                        exit()
                else:
                    self.droplets.append(drop)
                    drop.dropId = len(self.droplets)
                
                #Check for repeated droplets id
                repeat = True
                while repeat:
                    idList=[]
                    for drp in currentDroplets:
                        idList.append(drp.dropId)
                    number = self.find_repeat(idList)
                    if number:
                        print "duplicado"
                        drop1=[]
                        for drp in currentDroplets:
                            if drp.dropId == number:
                                if not drop1:
                                    drop1=drp
                                else:
                                    drop2=drp
                        print "drop1: " + str(drop1.dropId) + "centroid " + str(drop1.centroid)
                        print "drop2: " + str(drop2.dropId) + "centroid " + str(drop2.centroid)
                        if drop1.area > drop2.area:
                            self.droplets.append(drop2)
                            drop2.dropId = len(self.droplets)
                        else:
                            self.droplets.append(drop1)
                            drop1.dropId = len(self.droplets)
                        
                    else:
                        repeat=False
                        
                self.paths[drop.dropId].append(drop.centroid)   # mette una droplet nell'array
                cv2.drawContours(result, cnt, -1, (0, 255, 0), 1)
                ###message = "Color : " + str(color)
                message = "Id : " + str(drop.dropId)
                cv2.putText(result, message, centroid, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            
        self.previousDroplets = currentDroplets

    def do_track(self, frame, result):
        """ One shot: identify the droplet and display it in the image """

        # Image blurring and thresholding
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # RED TRACKING
        thresh_image = cv2.bitwise_or(cv2.inRange(frame_hsv, lower, upper), cv2.inRange(frame_hsv, lower2, upper2))
        cv2.blur(thresh_image, (5, 5), thresh_image)
        cv2.threshold(thresh_image, 0, 255, cv2.THRESH_OTSU, thresh_image)
        cv2.morphologyEx(thresh_image, cv2.MORPH_OPEN, (20, 20), thresh_image)
        cv2.morphologyEx(thresh_image, cv2.MORPH_CLOSE, (20, 20), thresh_image)
        ###cv2.imshow('Threshold Image', thresh_image)
        self.track(thresh_image, result, "red")
        
        
    def get_paths(self):
        return self.paths
        
    def show_output_biggest(self, result):
        maxArea=0
        biggestDrop=0
        for i, drp in enumerate(self.droplets):
            if drp.area > maxArea:
                maxArea = drp.area
                biggestDrop = drp.dropId
                starting_point = drp.path[0]
                lastPoint=len(self.paths[drp.dropId])-1
                ending_point = self.paths[drp.dropId][lastPoint]
        
#        for i, drp in enumerate(self.droplets):
#            for index, c in enumerate(self.paths[drp.dropId]):
#                cv2.circle(result, c, 1, (255, 0, 0), 1)
#                if index == 0:
#                    print('inizio', c)
#                    cv2.putText(result, "BEGIN", c, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
#                elif index == len(self.paths[drp.dropId]) - 1:
#                    print('fine', c)
#                    cv2.putText(result, "END", c, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
        for index, c in enumerate(self.paths[biggestDrop]):
            cv2.circle(result, c, 1, (255, 0, 0), 1)
            if index == 0:
                print('inizio', c)
                posText = (c[0], c[1]+11)
                cv2.putText(result, "BEGIN", posText, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
            elif index == len(self.paths[biggestDrop]) - 1:
                print('fine', c)
                posText = (c[0], c[1]+11)
                cv2.putText(result, "END", posText, cv2.FONT_HERSHEY_SIMPLEX, .5, 255)
        return starting_point, ending_point
    
    
    def show_output(self, result):

        for i, drp in enumerate(self.droplets):
            for index, c in enumerate(self.paths[drp.dropId]):
                cv2.circle(result, c, 1, (255, 0, 0), 1)
                if index == 0:
                    posText = (c[0], c[1]+8)
                    cv2.putText(result, "BEGIN " + str(drp.dropId), posText, cv2.FONT_HERSHEY_SIMPLEX, .3, 255)
                elif index == len(self.paths[drp.dropId]) - 1:
                    posText = (c[0], c[1]+8)
                    cv2.putText(result, "END " + str(drp.dropId), posText, cv2.FONT_HERSHEY_SIMPLEX, .3, 255)
        
        return

