class PetriDish:
    def __init__(self, _evobot, center, goalPos, diameter=110, edgeHeight = 20, volume=100, liquidType = 'undefined', worldCor=None):
        """
        This method initialize the petridish object.
        """
        self.center=center
        self.goalPos=goalPos
        self.diameter=diameter
        self.volume=volume
        self.liquidType=liquidType
        self.evobot = _evobot        
        self.dateLogger = None
        self.worldCor=worldCor
        self.edgeHeight = edgeHeight

