import math
import threading
import time

from configuration import *


class Head:
    """
    This class encapsulated methods for controlling the head of the robot
    """

    def __init__(self, _evobot):
        """
        This method initializes the head class. The key parameter is evobot
        to which this head is attached and the x and y limits of the robot
        given in mm.
        """
        self.evobot = _evobot

        # these are only updated upon receiving new positions form robot
        self.x = -1
        self.y = -1
        self.xOrg = -1  # Original x, before rounding
        self.yOrg = -1  # Original y, before rounding
        self.xLimit = HEAD['X_LIMIT']
        self.yLimit = HEAD['Y_LIMIT']
        self.headSpeed = 5000

        self.dataLogger = None
        self.evobot.heads.append(self)
        self.event = threading.Event()
        self.logBuffer = [0, 0, 0, 0, 0]
        self.logTitlesNeeded = True
        self.timeNow = None

    def _recvcb(self, line):
        """
        This is an internal use method that is used to parse messages from the robot
        """

        terms = []
        if line.startswith('X'):
            terms = str(line).split()
            self.xOrg = float(terms[6][0:])
            self.x = round(self.xOrg, 2)
            if terms[8].startswith('Z'):
                self.yOrg = float(terms[8][2:])
                self.y = round(self.yOrg, 2)
            self.newPositionAvailable = True
            if self.dataLogger is not None:
                self.timeNow = time.time() - self.evobot.iniTime
                xSpeed = (self.xOrg - self.logBuffer[1]) / (self.timeNow - self.logBuffer[0])
                ySpeed = (self.yOrg - self.logBuffer[2]) / (self.timeNow - self.logBuffer[0])
                distance = math.sqrt(
                    math.pow((self.xOrg - self.logBuffer[1]), 2) + math.pow((self.yOrg - self.logBuffer[2]), 2))
                speed = distance / (self.timeNow - self.logBuffer[0])
                acceleration = math.sqrt(
                    math.pow((xSpeed - self.logBuffer[3]), 2) + math.pow((ySpeed - self.logBuffer[4]), 2)) / (
                               self.timeNow - self.logBuffer[0])
                try:
                    if self.dataLogger.kind == 'dat':
                        self.dataLogger(str(self.timeNow) + ' ' + str(self.xOrg) + ' ' + str(self.yOrg) + "\n")
                    elif self.dataLogger.kind == 'csv':
                        if self.logTitlesNeeded:
                            self.dataLogger(('time', 'x', 'y', 'xSpeed', 'ySpeed', 'distance', 'speed', 'acceleration'))
                            self.logTitlesNeeded = False

                        self.dataLogger((str(self.timeNow), str(self.xOrg), str(self.yOrg), str(xSpeed), str(ySpeed),
                                         str(distance), str(speed), str(acceleration)))
                except:
                    pass

                self.logBuffer = [self.timeNow, self.xOrg, self.yOrg, xSpeed, ySpeed]

            self.event.set()

    def _updatePositionFromRobot(self):
        self.evobot.send('M114')
        self.event.wait()
        self.event.clear()

    def setDataLogger(self, logger):
        """
        This sets the data logger for the head. The data consist
        of the time, x, and y postions of the robot. The argument
        is a function that takes a string as input.
        """
        self.dataLogger = logger

    def park(self):
        """
        This method parks the head safely in preparation for shutdown
        of the robot. This is currently done by executing the home command.
        """
        self.home()

    def home(self):
        """
        This method homes the head by moving it to towards the switches.
        Once, the switches are engaged that position is defined to be (0,0).
        The method blocks the caller until the robot has completed
        the movement.
        """
        self.evobot._logUsrMsg('homing head...')
        self.evobot.send('G28')
        self._updatePositionFromRobot()
        while not (0.00 == self.x and 0.00 == self.y):
            self._updatePositionFromRobot()
        self.evobot._logUsrMsg('homed head')

    def move(self, x, y):
        """
        This method moves the head to the position (x,y) specified in mm.
        The method blocks the caller until the robot has completed
        the movement.
        """

        if x < 0 or x > self.xLimit or y < 0 or y > self.yLimit:
            self.evobot._logUsrMsg('head movements out of limits ' + str(x) + ", " + str(y))
            self.evobot.quit()

        x = round(x, 2)
        y = round(y, 2)
        self.evobot._logUsrMsg('head moving to: ' + str(x) + ' ' + str(y) + '...')

        moveToMsg = 'G1 X%f Z%f F%f' % (x, y, self.headSpeed)
        self.evobot.send(moveToMsg)
        self._updatePositionFromRobot()
        while not (x == self.x and y == self.y):
            self._updatePositionFromRobot()
        self.evobot._logUsrMsg('head moved')

    def moveContinously(self, x, y):
        """
        The method does not block the caller hence the robot may not have
        completed the movement before it returns.
        """

        if x <= 0 or x > self.xLimit or y < 0 or y > self.yLimit:
            self.evobot._logUsrMsg('head movements out of limits')
            self.evobot.quit()

        x = round(x, 2)
        y = round(y, 2)

        self.evobot._logUsrMsg('head moving to: ' + str(x) + ' ' + str(y) + '...')

        moveToMsg = 'G1 X%f Z%f' % (x, y)
        self.evobot.send(moveToMsg)
        if self.dataLogger is not None:
            try:
                if self.dataLogger.kind == 'dat':
                    self.dataLogger(
                        str(time.time() - self.evobot.iniTime) + ' ' + str(self.x) + ' ' + str(self.y) + "\n")
                elif self.dataLogger.kind == 'csv':
                    self.dataLogger((str(time.time() - self.evobot.iniTime), str(self.x), str(self.y)))
            except:
                pass

    def moveDiscrete(self, x, y, stepLength):

        """
        This method moves the head a stepLength towards position (x,y) unless the distance
        is short in which case it moves directly to (x,y). This is a none blocking call.
        """

        self.evobot._logUsrMsg('head moving discretely towards: ' + str(round(x, 2)) + ' ' + str(round(y, 2)) + '...')
        distance = math.pow(math.pow((x - self.x), 2) + math.pow((y - self.y), 2), .5)

        if stepLength > distance:  # we are close
            self.moveContinously(x, y)
        else:  # we are further away
            self.moveContinously(self.x + stepLength * (x - self.x) / distance,
                                 self.y + stepLength * (y - self.y) / distance)

        self.evobot._logUsrMsg('head moved discretely')

    def getX(self):
        """
        This returns the current x position of the robot
        """
        return self.x

    def getY(self):
        """
        This returns the current y position of the robot
        """
        return self.y

    def setSpeed(self, goalSpeed):
        """
        This method sets the speed of the head of the robot
        """
        if goalSpeed < 100 or goalSpeed > 9000:
            self.evobot._logUsrMsg('Head attempted to set its maximum speed to ' + str(
                goalSpeed) + 'mm/min, but exceed head speed\'s limits of [100:9000]')
            self.evobot.quit()

        goalSpeed = round(goalSpeed, 0)

        self.evobot._logUsrMsg('Setting head maximum speed to ' + str(goalSpeed) + 'mm/min')
        # moveToMsg = 'G1 F%f' % (goalSpeed)
        # self.evobot.send( moveToMsg )
        self.headSpeed = goalSpeed

        self.evobot._logUsrMsg('Plunger speed set')
